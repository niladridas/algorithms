//Authors:	Tohid Alizadeh and Sylvain Calinon, 2012
//			http://programming-by-demonstration.org

#include "DmpGmr.h"

int main(){
  vec in, out;
  
//  dMPGMR dmpGmrRwls(2, 5);                //A new object of class dMPGMR with nbStates=2, nbVar=5
//  dmpGmrRwls.init();                      //Initialization of the model
//  dmpGmrRwls.load("../bin/data/");                  //loading demonstration data from the specified path
//  dmpGmrRwls.learnWLS();                  //Learning model parameters using WLS
//  dmpGmrRwls.reproWLS(5, "../bin/data/Wls/");           //Generating new reproductions using WLS (nbRepro = 5)

  //Building a DMP model, learning the parameters and generating new reproductions using GMR
  dMPGMR dmpGmrRgmr(2, 5);                //A new object of class dMPGMR with nbStates=2, nbVar=5
  dmpGmrRgmr.init();                      //Initialization of the model
  dmpGmrRgmr.load("../bin/data/");                  //loading demonstration data from the specified path
  dmpGmrRgmr.learnGMR();                  //Learning model parameters using GMR
  in = linspace(0,0,1);                   //determining input variable(s) for GMR (in = 0)
  out = linspace(1,2,2);                  //Determining output variable(s) for GMR (out = [ 1 2])
  dmpGmrRgmr.reproGMR(5, in, out, "../bin/data/Gmr/");  //Generating new reproductions using GMR (nbRepro = 5)

  return 1;
}
