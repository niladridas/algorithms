/*
 * GMR_friction.cpp
 *
 *  Created on: 09-Feb-2015
 *      Author: nilxwam
 */

#include "DmpGmr.h"

int main(){
  vec in, out;

  dMPGMR dmpGmrRgmr(9, 5);                //A new object of class dMPGMR with 4 position 4 velocity 1 torque
  dmpGmrRgmr.init();                      //Initialization of the model
  dmpGmrRgmr.load("../bin/data/");                  //loading demonstration data from the specified path
  dmpGmrRgmr.learnGMR();                  //Learning model parameters using GMR
  in = linspace(0,0,1);                   //determining input variable(s) for GMR (in = 0)
  out = linspace(1,2,2);                  //Determining output variable(s) for GMR (out = [ 1 2])
  dmpGmrRgmr.reproGMR(5, in, out, "../bin/data/Gmr/");  //Generating new reproductions using GMR (nbRepro = 5)

  return 1;
}



