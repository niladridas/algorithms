//! This is a test for read_txt_fast class
/*!
 * read_txt_fast_test.cpp
 *
 *  Created on: 08-Feb-2015
 *      Author: nilxwam
 */
#include "read_txt_fast.h"

int main(int argc, char** argv) {

	int count_row;
	int total_row;
	int count_column;
	int prev_count_column;
	count_row = 1;
	count_column = 1;
	prev_count_column = 1;
	const char* filename = argv[1];
	read_txt_fast newcvsread;
	total_row = newcvsread.wc(filename);

	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> myMatrix;
	/*!
	 * Once the row.size() is called for the first time the number
	 * of columns required will be fixed.
	 */

	std::ifstream in(filename);
	if (in.fail())
		return (cout << "File not found" << endl) && 0;
	while (in.good()) {
		std::vector<std::string> row = newcvsread.csv_read_row(in, ',');
		if (count_row == 1) {
			count_column = row.size();
			myMatrix.resize(total_row, count_column);
		}
		if (prev_count_column != count_column && count_row != 1) {
			newcvsread.handle_error("data inconsistency");
		}

		for (int i = 0; i < count_column; i++) {
			myMatrix(count_row - 1, i) = atof((row[i]).c_str());
			cout << "[" << row[i] << "]" << "\t";
		}
		cout << endl;
		prev_count_column = count_column;
		count_row = count_row + 1;
	}
	in.close();
	std::cout << myMatrix << std::endl;

	return 0;
}

