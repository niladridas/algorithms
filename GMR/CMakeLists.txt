cmake_minimum_required(VERSION 2.6)

project(GMM)

find_library(EIGEN_LIB eigen $ENV{EIGEN_LIB_PATH})

include_directories(include)
include_directories (/usr/include/armadillo_bits)

add_definitions(-o2 -g)

link_directories (/usr/local/lib)

function(add_programs)
	foreach (prog ${ARGV})
		add_executable(build/${prog} src/${prog}.cpp)
	endforeach()
endfunction()

add_programs(
GMM_main
read_txt_fast_test
)

target_link_libraries (build/GMM_main armadillo)
