/*
 * read_txt_fast-inl.hpp
 *
 *  Created on: 08-Feb-2015
 *      Author: nilxwam
 */

#ifndef READ_TXT_FAST_INL_HPP_
#define READ_TXT_FAST_INL_HPP_

read_txt_fast::read_txt_fast() {

}

void read_txt_fast::handle_error(const char* msg) {
	perror(msg);
	exit(255);
}

uintmax_t read_txt_fast::wc(char const *fname) {
	static const int BUFFER_SIZE = 16 * 1024;
	int fd = open(fname, O_RDONLY);
	if (fd == -1)
		this->handle_error("open");

	/* Advise the kernel of our access pattern.  */
	posix_fadvise(fd, 0, 0, 1); // FDADVICE_SEQUENTIAL

	char buf[BUFFER_SIZE + 1];
	uintmax_t lines = 0;

	while (size_t bytes_read = read(fd, buf, BUFFER_SIZE)) {
		if (bytes_read == (size_t) -1)
			this->handle_error("read failed");
		if (!bytes_read)
			break;

		for (char *p = buf;
				(p = (char*) memchr(p, '\n', (buf + bytes_read) - p)); ++p)
			++lines;
	}

	return lines;
}

/*!The following two functions have been copied from http://www.zedwood.com/article/cpp-csv-parser */
std::vector<std::string> read_txt_fast::csv_read_row(std::string &line,
		char delimiter) {
	std::stringstream ss(line);
	return csv_read_row(ss, delimiter);
}

std::vector<std::string> read_txt_fast::csv_read_row(std::istream &in,
		char delimiter) {
	std::stringstream ss;
	bool inquotes = false;
	std::vector<std::string> row; //relying on RVO
	while (in.good()) {
		char c = in.get();
		if (!inquotes && c == '"') //beginquotechar
				{
			inquotes = true;
		} else if (inquotes && c == '"') //quotechar
				{
			if (in.peek() == '"') //2 consecutive quotes resolve to 1
					{
				ss << (char) in.get();
			} else //endquotechar
			{
				inquotes = false;
			}
		} else if (!inquotes && c == delimiter) //end of field
				{
			row.push_back(ss.str());
			ss.str("");
		} else if (!inquotes && (c == '\r' || c == '\n')) {
			if (in.peek() == '\n') {
				in.get();
			}
			row.push_back(ss.str());
			return row;
		} else {
			ss << c;
		}
	}
}

#endif /* READ_TXT_FAST_INL_HPP_ */
