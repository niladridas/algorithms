//Estimation of the parameters of a DMP (dynamic movement primitives) through GMR (Gaussian mixture regression).
//A DMP is composed of a virtual spring-damper system modulated by a non-linear force. The standard method to train 
//a DMP is to predefine a set of activations functions and estimate a set of force components through a weighted 
//least-squares (WLS) approach. The weighted sum of force components form a non-linear force perturbing the system, 
//by moving it away from the point-to-point linear motion while following a desired trajectory.
//GMR is used here to learn the joint distribution between the decay term s (determined by a canonical dynamical system) 
//and the non-linear force variable to estimate.
//Replacing WLS with GMR has the following advantages:
//It provides a probabilistic formulation of DMP (e.g., to allow the exploitation of correlation and variation information, 
//and to make the DMP approach compatible with other statistical machine learning tools).
//It simultaneously learns the non-linear force together with the activation functions. Namely, the Gaussian kernels 
//do not need to be equally spaced in time (or at predefined values of the decay term 's'), and the bandwidths (variance of 
//the Gaussians) are automatically estimated from the data instead of being hand-tuned.
//It provides a more accurate approximation of the non-linear perturbing force with local linear models of degree 1 
//instead of degree 0 (by exploiting the conditional probability properties of Gaussian distributions).
//
//Reference: Calinon, S., Li, Z., Alizadeh, T., Tsagarakis, N.G. and Caldwell, D.G. (2012) Statistical dynamical systems 
//for skills acquisition in humanoids. Proc. of the IEEE Intl Conf. on Humanoid Robots (Humanoids).
//
//Authors:	Tohid Alizadeh and Sylvain Calinon, 2012
//			http://programming-by-demonstration.org
//	
//This source code is given for free! In exchange, we would be grateful if you cite
//the following reference in any academic publication that uses this code or part of it:
//
//@inproceedings{Calinon12Hum,
//	author="Calinon, S. and Li, Z. and Alizadeh, T. and Tsagarakis, N. G. and Caldwell, D. G.",
//	title="Statistical dynamical systems for skills acquisition in humanoids",
//	booktitle="Proc. {IEEE} Intl Conf. on Humanoid Robots ({H}umanoids)",
//	year="2012",
//	address="Osaka, Japan"
//}

#ifndef DMPGMR_INL_HPP_
#define DMPGMR_INL_HPP_
//--------------------------------------------------------------
dMPGMR::dMPGMR(){
}

//--------------------------------------------------------------
dMPGMR::dMPGMR(int n, int s){
  model.nbStates = s;   //Number of States of the model
  model.nbVar = n;  //2 for (x,y) or 3 for (t, x, y) or ...
  model.kP = 100;
  model.nbData = 200;
  model.kV = 20;
  model.dt = 0.01;
  model.alpha = 1;
};

//--------------------------------------------------------------
int dMPGMR::init(){
  double stdS;
  cout<<endl<<"initialization of the model is started"<<endl;
  model.Mud = linspace(model.nbData, 1, model.nbStates);
  model.SigmaD = 2000;
  model.MuS = exp(-model.alpha*model.Mud*model.dt);
  model.SigmaS = zeros(model.nbStates,1);
  for(int i=0; i<model.nbStates; i++){
    stdS = model.MuS(i)-exp(-model.alpha*(model.Mud(i)+sqrt(model.SigmaD))*model.dt);
    model.SigmaS(i) = stdS*stdS;
  }
  cout<<"initialization of the model is finished"<<endl;
  return 1;
}

//--------------------------------------------------------------
int dMPGMR::load(string path){
  mat DataM;
  string UpdatedPath;

  UpdatedPath = path+"data_obs.txt";
  DataM.load(UpdatedPath,raw_ascii);
  addDemo(DataM);
  cout<<"Loading of the demonstartions is finished"<<endl;
  return 1;




}

//--------------------------------------------------------------
int dMPGMR::addDemo(mat DataM){
  model.xT = zeros(model.nbVar,1);
  model.Data = trans(DataM);
  model.posId = linspace(0,model.nbVar-1, model.nbVar);
  model.velId = linspace(model.nbVar, 2*model.nbVar-1, model.nbVar);
  model.accId = linspace(2*model.nbVar, 3*model.nbVar-1, model.nbVar);
  model.xT = model.Data.submat(model.posId(0) , model.nbData-1, model.posId(model.nbVar-1), model.nbData-1);
  model.Data.rows(model.velId(0), model.velId(model.nbVar-1)) =
            computederivative(model.Data.rows(model.posId(0), model.posId(model.nbVar-1)), model.dt);
  model.Data.rows(model.accId(0), model.accId(model.nbVar-1)) =
            computederivative(model.Data.rows(model.velId(0), model.velId(model.nbVar-1)), model.dt);
  return 1;
}

//--------------------------------------------------------------
mat dMPGMR::computederivative(mat Data, double dt){
  mat rData = zeros(model.nbVar, model.nbData);
  rData.cols(0,model.nbData-2) = Data.cols(1, model.nbData-1);
  rData.col(model.nbData-1) = Data.col(model.nbData-1);
  rData -= Data;
  rData = rData/model.dt;

  return rData;
}

//--------------------------------------------------------------
int dMPGMR::learnWLS(){
  double s=1;
  model.Y = zeros(model.nbVar, model.nbData);
  model.H = zeros(model.nbData, model.nbStates);
  model.MuF = zeros(model.nbVar, model.nbStates);
  vec h = zeros(model.nbStates,1);
  cout<<"Start to learn using WLS"<<endl;
  model.sList = zeros(model.nbData,1), h = zeros(model.nbStates,1);
  for (int n=0; n<model.nbData; n++){
    s += -model.alpha*s*model.dt;
    model.sList(n) = s;
    for (int i=0; i<model.nbStates; i++){
      h(i) = GaussPDF(s, model.MuS(i), model.SigmaS(i));
    }
    model.H.row(n) = trans(h/sum(h));
  }
  model.Y = model.Data.rows(model.accId(0), model.accId(model.nbVar-1))
    - ((repmat(model.xT,1,model.nbData)-model.Data.rows(model.posId(0), model.posId(model.nbVar-1)))*model.kP
    - model.Data.rows(model.velId(0), model.velId(model.nbVar-1))*model.kV);
  model.MuF = trans(inv(trans(model.H)*model.H)*trans(model.H)*trans(model.Y));

  cout<<"Learning using WLS is finished"<<endl;
  return 1;
}

//--------------------------------------------------------------
int dMPGMR::learnGMR(){
  double s = 1;
  model.Y = zeros(model.nbVar, model.nbData);
  model.sList = zeros(model.nbData,1);
  for (int n=0; n<model.nbData; n++){
    s += -model.alpha*s*model.dt;
    model.sList(n) = s;
  }
  model.Y = model.Data.rows(model.accId(0), model.accId(model.nbVar-1))
    - ((repmat(model.xT,1,model.nbData)-model.Data.rows(model.posId(0), model.posId(model.nbVar-1)))*model.kP
    - model.Data.rows(model.velId(0), model.velId(model.nbVar-1))*model.kV);
  model.Gmm.prior = zeros(model.nbStates,1);
  model.Gmm.Mu = zeros(model.nbVar, model.nbStates);
  model.Gmm.Sigma.resize(model.nbStates);

  EmInitRegularTiming();
  EmBoundingCov();

  return 1;
}

//--------------------------------------------------------------
int dMPGMR::EmInitRegularTiming(){
  mat DataEM = zeros(model.nbVar+1, model.nbData), MuTmp, Mu, DataTmp;
  int nbVarTmp = model.nbVar+1;
  DataEM.row(0) = trans(model.sList);
  DataEM.rows(1, model.nbVar) = model.Y;
  vec Priors = zeros<vec>(model.nbStates);
  vector<mat> Sigma;
  Mu = zeros(nbVarTmp, model.nbStates);
  Sigma.resize(model.nbStates);
  vec TimingSep = linspace(min(DataEM.row(0)), max(DataEM.row(0)), model.nbStates+1);
  vec IdTmp = zeros<vec>(DataEM.n_cols);
  int k;
  DataTmp = zeros(DataEM.n_rows, DataEM.n_cols);
  for(int i=0; i<model.nbStates; i++){
    MuTmp = zeros(nbVarTmp,1);
    k =0;
    for (int j=0; j<model.nbData; j++){
      if(DataEM(0,j)>=TimingSep(i)&&DataEM(0,j)<TimingSep(i+1)){
        IdTmp(k) = j;
        DataTmp.col(k) = DataEM.col(j);
        MuTmp += DataEM.col(j);
        k+=1;
      }
    }
    Mu.col(i) = MuTmp/k;
    Sigma[i] = cov(trans(DataTmp.cols(0,k-1)));
    Priors(i) = k;
  }
  Priors = Priors/sum(Priors);
  model.Gmm.prior = Priors;
  model.Gmm.Mu = Mu;
  model.Gmm.Sigma = Sigma;

  return 1;
}

//--------------------------------------------------------------
int dMPGMR::EmBoundingCov(){
  mat DataEM = zeros(model.nbVar+1, model.nbData);
  DataEM.row(0) = trans(model.sList);
  DataEM.rows(1, model.nbVar) = model.Y;
  if(model.nbStates<1)
    return -1;
  //Thresholds for the EM iterations
  int nbMaxSteps = 100;  //maximum # of iterations
  int nbMinSteps = 10;  //minimum # of iterations
  float maxDiffLL = 1E-50;
  float diagRegularizationFactor = 1E-4;
  int nbVarTmp, nbData;
  nbData = DataEM.n_cols;
  nbVarTmp = DataEM.n_rows;
  vec sumTmp = zeros(nbData,1), LL_Tmp = zeros(nbData,1);
  mat E = zeros(1, model.nbStates), Pxi, PixTmp, Pix;
  Pxi = zeros(nbData,model.nbStates);
  PixTmp = Pxi;
  Pix = Pxi;
  mat DataTmp, LL(1,nbMaxSteps);

  cout<<"Start to learn using EM Bounding Cov"<<endl;
  for(int nbIter=0;  nbIter<nbMaxSteps; nbIter++){
    // ------------ E step -----------------
    for (int i=0; i<model.nbStates; i++){
      //Compute probability p(x|i)
      Pxi.col(i) = GaussPDF(DataEM, model.Gmm.Mu.col(i), model.Gmm.Sigma[i]);
    }
    //Compute posterior probability p(i|x)
    PixTmp = repmat(trans(model.Gmm.prior), nbData, 1)%Pxi;
    Pix = PixTmp/repmat(sum(PixTmp,1),1, model.nbStates);
    //Compute cumulated posterior probability
    E = sum(Pix,0);
    // ----------- M step -------------
    for(int i=0;i<model.nbStates;i++){
      //Update priors
      model.Gmm.prior(i) = E(i)/nbData;
      //Update the centers
      model.Gmm.Mu.col(i) = DataEM*Pix.col(i)/E(i);
      //Update the covariance matrices
      DataTmp = DataEM - repmat(model.Gmm.Mu.col(i),1,nbData);
      model.Gmm.Sigma[i] = (repmat(trans(Pix.col(i)),nbVarTmp,1)%DataTmp*trans(DataTmp))/E(i);
      //Add a tiny variance to avoid numerical instability
      model.Gmm.Sigma[i] += diagRegularizationFactor*diagmat(ones(nbVarTmp,1));
    }
    // Stopping Criteria
    for (int i=0; i<model.nbStates; i++){
      //Compute probability p(x|i)
      Pxi.col(i) = GaussPDF(DataEM, model.Gmm.Mu.col(i), model.Gmm.Sigma[i]);
    }
    // ====== Computing the average log-likelihood through the ALPHA scaling factors
    LL(nbIter)=0;
    sumTmp = Pxi*model.Gmm.prior;
    LL_Tmp = log(sumTmp);
    LL(nbIter) = sum(LL_Tmp);
    LL(nbIter)/= nbData;
    //Stop the algorithm if EM converged
    if(nbIter>nbMinSteps && (LL(nbIter)-LL(nbIter-1))<maxDiffLL){
      cout<<"EM converged after  "<< nbIter<<"  iterations."<<endl;
      break;
    }
    if(nbIter==nbMaxSteps-1)
      cout<<"The maximum number of  "<<nbMaxSteps<< "  EM iterations has been reached."<<endl;
  }
  return 1;
}

//--------------------------------------------------------------
int dMPGMR::reproWLS(int nbRepro, string path){
  double s;//Decay term
  mat currF0(model.nbVar,1);
  string UpdatedPath;
  stringstream pathTmp;

  model.Repro.resize(nbRepro);
  cout<<"Reproductions using WLS ...."<<endl<<endl;
  for (int n=0; n<nbRepro; n++){
    model.Repro[n].Data = zeros(model.nbVar*3, model.nbData);
    model.Repro[n].H = zeros(model.nbStates, model.nbData);
    model.Repro[n].F = zeros(model.nbVar, model.nbData);

    if (n==0){
      model.Repro[n].currPos = model.Data.submat(model.posId(0),0,model.posId(model.nbVar-1),0); //Initial Position
    }
    else{
      model.Repro[n].currPos = model.Data.submat(model.posId(0),0,model.posId(model.nbVar-1),0) + (randu(2,1)-.5)*5; //Initial position with noise
    }
    model.Repro[n].currVel = zeros(model.nbVar,1);  //Initial Velocity
    model.Repro[n].currAcc = zeros(model.nbVar,1);  //Initial Velocity
    s = 1; //Decay term
    for (int j = 0; j<model.nbData; j++){
      //Log data
      model.Repro[n].Data.submat(model.posId(0),j,model.posId(model.nbVar-1),j) = model.Repro[n].currPos;
      model.Repro[n].Data.submat(model.velId(0),j,model.velId(model.nbVar-1),j) = model.Repro[n].currVel;
      model.Repro[n].Data.submat(model.accId(0),j,model.accId(model.nbVar-1),j) = model.Repro[n].currAcc;
      //Update s (ds=-alpha*s)
      s = s + (-model.alpha*s)*model.dt;
      //Activation weights with WLS
      for (int i=0; i<model.nbStates; i++){
        model.Repro[n].H(i,j) = GaussPDF(s, model.MuS(i), model.SigmaS(i));
      }
      model.Repro[n].H.col(j) /= sum(model.Repro[n].H.col(j));
      //Evaluate acceleration with WLS
      currF0 = (model.xT - model.Repro[n].currPos)*model.kP - model.Repro[n].currVel*model.kV;
      model.Repro[n].F.col(j) = model.MuF*model.Repro[n].H.col(j);
      model.Repro[n].currAcc = currF0 + model.Repro[n].F.col(j);
      //Update velocity
      model.Repro[n].currVel += model.Repro[n].currAcc*model.dt;
      //Update position
      model.Repro[n].currPos += model.Repro[n].currVel*model.dt;
    }
    // Saving the reproduction data
    pathTmp<<n+1;
    UpdatedPath = path + "data_rep" + pathTmp.str() + ".txt";
    model.Repro[n].Data.save(UpdatedPath, raw_ascii);
    pathTmp.str("");
  }
  return 1;
}

//--------------------------------------------------------------
int dMPGMR::reproGMR(int nbRepro, vec in, vec out, string path){
  double s;//Decay term
  mat currF0(model.nbVar,1), currFTmp(model.nbVar,1);
  stringstream pathTmp;
  string UpdatedPath;
  cout<<"Reproductions using GMR ..."<<endl<<endl;
  model.Repro.resize(nbRepro);
  for (int n=0; n<nbRepro; n++){
    model.Repro[n].Data = zeros(model.nbVar*3, model.nbData);
    model.Repro[n].H = zeros(model.nbStates, model.nbData);
    model.Repro[n].F = zeros(model.nbVar, model.nbData);

    if (n==0){
      model.Repro[n].currPos = model.Data.submat(model.posId(0),0,model.posId(model.nbVar-1),0); //Initial Position
    }
    else{
      model.Repro[n].currPos = model.Data.submat(model.posId(0),0,model.posId(model.nbVar-1),0) + (randu(2,1)-.5)*5; //Initial position with noise
    }
    model.Repro[n].currVel = zeros(model.nbVar,1);  //Initial Velocity
    model.Repro[n].currAcc = zeros(model.nbVar,1);  //Initial Velocity
    s = 1; //Decay term
    for (int j = 0; j<model.nbData; j++){
      //Log data
      model.Repro[n].Data.submat(model.posId(0),j,model.posId(model.nbVar-1),j) = model.Repro[n].currPos;
      model.Repro[n].Data.submat(model.velId(0),j,model.velId(model.nbVar-1),j) = model.Repro[n].currVel;
      model.Repro[n].Data.submat(model.accId(0),j,model.accId(model.nbVar-1),j) = model.Repro[n].currAcc;
      //Update s (ds=-alpha*s)
      s  += (-model.alpha*s)*model.dt;
      //Activation weights with GMR
      for (int i=0; i<model.nbStates; i++){
        model.Repro[n].H(i,j) = model.Gmm.prior(i)
        *GaussPDF(s, as_scalar(model.Gmm.Mu.submat(in(0),i,in(in.n_rows-1),i)), as_scalar(model.Gmm.Sigma[i].submat(in(0), in(0), in(in.n_rows-1), in(in.n_rows-1))));
      }
      model.Repro[n].H.col(j) /= sum(model.Repro[n].H.col(j));
      //Evaluate acceleration with GMR
      currF0 = (model.xT - model.Repro[n].currPos)*model.kP - model.Repro[n].currVel*model.kV;
      model.Repro[n].F.col(j) = zeros(model.nbVar,1);
      for(int i=0; i<model.nbStates; i++){
        currFTmp = model.Gmm.Mu.submat(out(0),  i, out(out.n_rows-1), i) +
        model.Gmm.Sigma[i].submat(out(0), in(0), out(out.n_rows-1), in(in.n_rows-1))* inv(model.Gmm.Sigma[i].submat(in(0), in(0), in(in.n_rows-1), in(in.n_rows-1)))*
        (s-model.Gmm.Mu.submat(in(0),  i, in(in.n_rows-1), i));
        model.Repro[n].F.col(j) += model.Repro[n].H(i,j)*currFTmp;
      }
      model.Repro[n].currAcc = currF0 + model.Repro[n].F.col(j);
      //Update velocity
      model.Repro[n].currVel += model.Repro[n].currAcc*model.dt;
      //Update position
      model.Repro[n].currPos += model.Repro[n].currVel*model.dt;
    }
    //Saving the reproduction in to text files
    pathTmp<<n+1;
    UpdatedPath = path + "data_rep" + pathTmp.str() + ".txt";
    model.Repro[n].Data.save(UpdatedPath, raw_ascii);
    pathTmp.str("");
  }
  return 1;
}

//--------------------------------------------------------------
vec dMPGMR::GaussPDF(mat M, vec Mu, mat Sigma){
  mat D_Tmp = trans(M) - repmat(trans(Mu),M.n_cols,1);
  mat invTmp;
  if (inv(invTmp,Sigma)){
    vec Probs = sum(D_Tmp%((D_Tmp)*(invTmp)), 1);
    Probs = exp(-0.5*Probs) / (sqrt(pow((2*PI),Sigma.n_cols) * (abs(det(Sigma))+REALMIN))+REALMIN);
    return Probs;
  }
  else{
    cout<<"Error: INV failed!!!2"<<endl;
    vec Probs(M.n_cols);
    Probs=REALMIN;
    return Probs;
  }
}

//--------------------------------------------------------------
double dMPGMR::GaussPDF(vec M, vec Mu, mat Sigma){
  mat D_Tmp = trans(M)-trans(Mu);
  mat invTmp=Sigma;
  if (inv(invTmp,Sigma)){
    double Probs = as_scalar(sum(D_Tmp%((D_Tmp)*(invTmp)), 1));
    Probs = exp(-0.5*Probs) / (sqrt(pow((2*PI),Sigma.n_cols) * (abs(det(Sigma))+REALMIN))+REALMIN);
    return Probs;
  }
  else{
    cout<<"Sigma ="<<endl<<Sigma<<endl;
    cout<<"Error: INV failed!!!11"<<endl;
    return REALMIN;
  }
}

//--------------------------------------------------------------
double dMPGMR::GaussPDF(double M, double Mu, double Sigma){
  double D_Tmp = M-Mu;
  double Probs = abs((D_Tmp*D_Tmp)/((Sigma)+REALMIN));
  Probs = exp(-0.5*Probs) / (sqrt(abs(pow((2*PI),model.nbVar)*(Sigma)))+REALMIN);
  return Probs;
}
#endif DMPGMR_INL_HPP_
