//! A class definition
/*!
 * This header file contains definition of a class
 * which is used to load data from files into eigen(library) matrices.
 * For now it gives support to comma and space separated data.
 */
#ifndef READ_TXT_FAST_H_
#define READ_TXT_FAST_H_

#include <fcntl.h>
#include <iostream>
#include <stdint.h>
#include <string.h>
#include <cstdlib>
#include <iostream>

#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <istream>

#include <eigen3/Eigen/Core> //! Eigen 3 is used

using std::cout;
using std::endl;

class read_txt_fast {

public:

public:

	//! Public member function
	/*!
	 * Member function definition is given here. The declarations
	 * are in read_txt_fast-inl.hpp
	 */
	read_txt_fast(); /*! constructor */
	uintmax_t wc(char const *fname);
	void handle_error(const char* msg); /*! prints error msg */
	std::vector<std::string> csv_read_row(std::istream &in, char delimiter);
	std::vector<std::string> csv_read_row(std::string &in, char delimiter);


};

/*!
 * This is the header where the definitions of the functions are given
 */
#include <Detail/read_txt_fast-inl.hpp>
#endif /* READ_TXT_FAST_H_ */
