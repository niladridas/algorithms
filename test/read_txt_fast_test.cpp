//! This is a test for read_txt_fast class
/*!
 * read_txt_fast_test.cpp
 *
 *  Created on: 08-Feb-2015
 *      Author: nilxwam
 */
#include "read_txt_fast.h"


int main(int argc, char** argv)
{
    std::ifstream in("input.csv");
    if (in.fail()) return (cout << "File not found" << endl) && 0;
    while(in.good())
    {
        std::vector<std::string> row = csv_read_row(in, ',');
        for(int i=0, leng=row.size(); i<leng; i++)
            cout << "[" << row[i] << "]" << "\t";
        cout << endl;
    }
    in.close();

    std::string line;
    in.open("input.csv");
    while(getline(in, line)  && in.good())
    {
        std::vector<std::string> row = csv_read_row(line, ',');
        for(int i=0, leng=row.size(); i<leng; i++)
            cout << "[" << row[i] << "]" << "\t";
        cout << endl;
    }
    in.close();

    return 0;
}


