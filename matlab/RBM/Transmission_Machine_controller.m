% Transmission Robot to Controller

function U_pd = Transmission_Machine_controller(U_p) 
global T_s;
global tick;
global U_p_discrete_delayed[20000];
global U_p_discrete[20000]; 
% U_pd is discrete and U_p is continious
% First step is tp change the U_p(t) to the corresponding U_p[i] i.e.
% continious to discrete
% The input U_p will be a vector with proper time stamp from start of a
% time period to the start of the next time period.
% T is a vector containing the time stamps between (i-1)*T_s to i*T_s

% Difference Vector

U_p_discrete[tick] = sqrt(T_s)*U_p[tick];
U_p_discrete_delayed[tick + p_i] =  U_p_discrete[tick];

end