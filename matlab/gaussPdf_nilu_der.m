function outPdf_der =  gaussPdf_nilu_der(Data,Data_der, Mu, Sigma,outPdf)
[nbVar,nbData] = size(Data);
Data = Data' - repmat(Mu',nbData,1);
outPdf_der = outPdf.*sum((Data*inv(Sigma)).*Data_der, 2);
end