% Function for computing probability of multivariate data given the mean
% and co-variance matrix
function outPdf = gaussPdf_nilu(Data, Mu, Sigma)
    % The function takes N_states x N_data_points as Data
    % mean as N_states x k
    % Variance as N_states x N_states
    % outPdf is 1 X N_data_points
%     [numStates,numData] = size(Data);
%     outPdf = zeros(1,numData);
%     for i = 1:numData
%     tmp_var =  ((2*pi)^numStates)*(abs(det(Variance_data))+realmin);
%     outPdf(1,i) = (1/sqrt(tmp_var))...
%         * exp(-0.5) * exp((Data(:,i) - Mean_data)'*inv(Variance_data)*(Data(:,i) - Mean_data));
%     end
[nbVar,nbData] = size(Data);

Data = Data' - repmat(Mu',nbData,1);
outPdf = sum((Data*inv(Sigma)).*Data, 2);
outPdf = exp(-0.5*outPdf) / sqrt((2*pi)^nbVar * (abs(det(Sigma))+realmin));
