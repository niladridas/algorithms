% grad V
function grad_V =  gradient_V(P_t,eta_t,eta_star_t)
% global eta_row;
% global L;
n_t = size(eta_t,1);
[row_P_t,col_P_t] = size(P_t);
L_t = ((row_P_t - col_P_t)/(col_P_t+1));
% L_t = L;
grad_V_1 = zeros(n_t,1);
grad_V_0 = (P_t(1:n_t,1:n_t)+P_t(1:n_t,1:n_t)')*(eta_t-eta_star_t);
if(L_t ~= 0)
for i = 1:L_t
    if ((eta_t - eta_star_t)'*P_t(n_t*i+1:n_t*i+n_t,1:n_t)*...
            (eta_t - eta_star_t - P_t(n_t*L_t+n_t+i)') >= 0)
    grad_V_1 = grad_V_1 + 2*(eta_t - eta_star_t)'*P_t(n_t*i+1:n_t*i+n_t,1:n_t)*...
        (eta_t - eta_star_t - P_t(n_t*L_t+n_t+i,:)')*...
        ((P_t(n_t*i+1:n_t*i+n_t,1:n_t)+P_t(n_t*i+1:n_t*i+n_t,1:n_t)')*((eta_t - eta_star_t))+...
        P_t(n_t*i+1:n_t*i+n_t,1:n_t)*P_t(n_t*L_t+n_t+i,:)'); 
    end
end
end
grad_V = grad_V_0 + grad_V_1;