% Real testing 
% We now have the eta_dot_estimate, V and the input 
% We are able to generate next eta given the instantaneous values of eta
clc;
clear all
% We are provided with a begining input as eta_start
load('Priors_learnt'); % this will load variable Priors
load('Mu_learnt'); % this will load variable Mu
load('Sigma_learnt'); % this will load variable Sigma
load('P_values'); % this will load variable x
load('eta_star_mat') % this will load eta_star;
load('validating_data_mat'); % loading a validation data

%  Priors = load('priors.txt'); % this will load variable Priors
%  Mu = load('mean.txt'); % this will load variable Mu
%  tmp_Sigma = load('sigma.txt'); % this will load variable Sigma
% x = load('P0.txt'); % this will load variable x
% load('eta_star_mat') % this will load eta_star;
% load('validating_data_mat'); % loading a validation data

% for i = 1:7
% Sigma(:,:,i) = tmp_Sigma((i-1)*6+1:i*6,:);
% end
 


global rho_0;
global K_0;
global delta_t;

delta_t = 0.001; %-------------------------------------------------------------------------------------
rho_0 = 0.01; %----------------------------------------------------------------------------------------
K_0 = 0.01; %------------------------------------------------------------------------------------------

eta_start = [0.1961;0;0.1457];
%eta_start = validating_data(1:3,1);
eta_tmp = eta_start;
rows_eta = size(eta_tmp,1);
output_solutions = [];
% match the present case
eta_dot_tmp = ones(rows_eta,1);   % vector where each is a very large number
count_solution = 1;
temp_final_eta_dot = 1;

 while(norm(temp_final_eta_dot)>=0.01 && norm(eta_tmp - eta_star)>=0.01)

    output_solutions(count_solution,:) = eta_tmp;
    grad_at_eta = gradient_V(x,eta_tmp,eta_star);
    [y1, Sigma_y1]= GMR(Priors, Mu, Sigma,eta_tmp,1:(rows_eta),(rows_eta+1):(rows_eta*2) );
    eta_dot_tmp = y1;
    output_input(:,count_solution) = input_U(grad_at_eta,eta_dot_tmp,eta_tmp, eta_star);
    final_eta_dot(:,count_solution) = eta_dot_tmp + input_U(grad_at_eta,eta_dot_tmp,eta_tmp, eta_star);
    temp_final_eta_dot =final_eta_dot(:,count_solution); 
    eta_tmp = eta_tmp + delta_t*final_eta_dot(:,count_solution);
    count_solution = count_solution + 1;   
end