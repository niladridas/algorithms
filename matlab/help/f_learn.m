all_data= load('abc_out');
Filtered_data = all_data(:,2:end);

%% ----------------------------------------------------------------------------------------------------------%%
[row_Filtered_data, col_Filtered_data] = size(Filtered_data);

% Restructuring the data as first the state data as (no. of states X ...
% Total time stamp) and immediately below we have the state derivatives ...
% in the same fashion as the states

Data_operate = Filtered_data';

% The number of Gaussian components used is hard coded
nogaussians = input('Total number of gaussians to be used ');

% % % Calculating the Joint Probability Distribution Model

% % Using the k-means clustering algorithm to find the prior values of ...
% % the distribution model. The prior values are (component probabilities,
% % mean and covariance matrices)
% % Copied from http://programming-by-demonstration.org

[noVar,noData] = size(Data_operate);
[Data_id,Centres] = kmeans(Data_operate', nogaussians);
Mu_k = Centres';
for i=1:nogaussians
  idtmp = find(Data_id==i);
  Priors_k(i) = length(idtmp);
  Sigma_k(:,:,i) = cov([Data_operate(:,idtmp) Data_operate(:,idtmp)]');
  %Add a tiny variance to avoid numerical instability
  Sigma_k(:,:,i) = Sigma_k(:,:,i) + 1E-5.*diag(ones(noVar,1));
end
Priors_k = Priors_k ./ sum(Priors_k);


% % Expectation Maximization Algorithm
% % Copied from http://programming-by-demonstration.org
% % Criterion to stop the EM iterative update

[Priors1, Mu1, Sigma1] = EM(Data_operate, Priors_k, Mu_k, Sigma_k);

% % Creation of a mixed model
Priors = Priors1;
Mu = Mu1;
Sigma = Sigma1;
%%
save('Priors_learnt','Priors');
% fileIPrior = fopen('priors.txt','w');
% % This will depend upon the number of gaussians manually choosen
% priorstring = '%f';
% for i = 1:nogaussians-2
%    priorstring = strcat(priorstring,' %f'); 
% end
% priorstring = strcat(priorstring,' %f\n'); 
% fprintf(fileIPrior,priorstring,Priors);
dlmwrite('priors.txt', Priors, 'delimiter', '\t', 'precision', 32)
%save('priors.txt','Priors','-ascii','-double');
save('Mu_learnt','Mu');
% fileIMean = fopen('mean.txt','w');
% % This will depend upon the number of gaussians manually choosen
% mustring = '%f';
% for i = 1:nogaussians-2
%    mustring = strcat(mustring,' %f'); 
% end
% mustring = strcat(mustring,' %f\n'); 
% fprintf(fileIMean,mustring,Mu);
dlmwrite('mean.txt', Mu, 'delimiter', '\t', 'precision', 32)
%save('mean.txt','Mu','-ascii','-double');

save('Sigma_learnt','Sigma');
% fileISigma = fopen('sigma.txt','w');
% fprintf(fileISigma,'%f %f %f %f %f %f\n',Sigma);
tmp_sigma = [];
for i = 1:nogaussians
tmp_sigma = [tmp_sigma;Sigma(:,:,i)];
end
%save('sigma.txt','tmp_sigma','-ascii','-double');
dlmwrite('sigma.txt', tmp_sigma, 'delimiter', '\t', 'precision', 32)
% X_M is the query point of the input
% % Optional just for checking
% [expData(1:noVar/2,:),expSigma] = GMR(Priors, Mu, Sigma,  eta, 1:3, 4:6);
% Optional just for checking with validation data
[expData(1:3,:),expSigma] = GMR(Priors, Mu, Sigma,  Data_operate(1:5,:), 1:5, 6:8);
% Writing sysconfig.txt file
% filesysconfig = fopen('sysconfig.txt','w');
% fprintf(filesysconfig,'num_input=5\n');
% fprintf(filesysconfig,'num_output=3\n');
% fprintf(filesysconfig,'num_lyp_asym_copont=0\n');
% fprintf(filesysconfig,'xi_star_training=[%f;%f;%f]', eta_star(1),eta_star(2),eta_star(3));

figure(1)
plot(expData(1,:));
hold on ;
plot(Data_operate(6,:),'r');
figure(2)
plot(expData(2,:));
hold on ;
plot(Data_operate(7,:),'r');
figure(3)
plot(expData(3,:));
hold on ;
plot(Data_operate(8,:),'r');


[m,n]=size(all_data); % Determine size of data
p=zeros(2,1); % save index of consecutive break point denoting change in demonstrations
p(1,1) = 0;
count = 0;
temp_cell={};% define cell to store diff demonstrations
for i=1:m-1 
   if all_data(i+1,1) < all_data(i,1)
       count = count +1;
        p(2,1)=i;
        temp_cell{count,1} = all_data(p(1,1)+1:p(2,1),2:9); % considering the first two joint angles
        p(1,1)=i;
   end
    if i == m-1
          temp_cell{count+1,1} = all_data(p(1,1)+1:m,2:9); 
          end
    i=i+1;
end

[total_no_demo, col_temp_cell] = size(temp_cell);
fprintf('Number of demonstrations %i\n', total_no_demo);
valid_demo_no = input('enter which demonstration to be taken for validation  ');

training_data = [];

for i=1:total_no_demo
	if i~= valid_demo_no 
        training_data = cat(2,training_data,temp_cell{i}');
    elseif i== valid_demo_no
		validating_data =  temp_cell{valid_demo_no}';
    end
end
save('validating_data_mat','validating_data');