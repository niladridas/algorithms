% Non linear constraints
function [c,ceq] = positive_def(P_tt)
ceq = [];
c = 1;
% P_tt
% display(P_tt)
[row_P_tt,col_P_tt] = size(P_tt);
L_tt = (row_P_tt - col_P_tt)/(col_P_tt+1);
% c = zeros(L_tt+1,1);
eig_val = ones(L_tt+1,col_P_tt);
real_part = ones(L_tt+1,col_P_tt);
for i = 0:L_tt
    eig_val(i+1,:) = eig(P_tt(col_P_tt*i + 1:col_P_tt*i + col_P_tt,1:col_P_tt));
    real_part(i+1,:) = real(eig_val(i+1,:));
end
display(min(min(real_part)));
if (min(min(real_part)) > 10^(-1))
   c = -1;
if (min(min(real_part)) <= 10^(-1))
   c = 1;
end
% display(c);
% display(ceq);
end