% function to calculate the value of psi at a particular value of eta and
% eta_dot
function psi = cost_psi(P_t,eta_t,eta_dot_t,eta_star_t,L_t,n_t)
% eta and eta_dot are vectors
grad_V_1 = zeros(n_t,1);
grad_V_0 = (P_t(1:n_t,1:n_t)+P_t(1:n_t,1:n_t)')*(eta_t-eta_star_t);
if(L_t~=0)
for i = 1:L_t
    if ((eta_t - eta_star_t)'*P_t((n_t*i+1):(n_t*i+n_t),1:n_t)*...
            (eta_t - eta_star_t - P_t(n_t*L_t+n_t+i,:)') >= 0)
    grad_V_1 = grad_V_1 + 2*(eta_t - eta_star_t)'*P_t(n_t*i+1:n_t*i+n_t,1:n_t)*...
        (eta_t - eta_star_t - P_t(n_t*L_t+n_t+i,:)')*...
        ((P_t(n_t*i+1:n_t*i+n_t,1:n_t)+P_t(n_t*i+1:n_t*i+n_t,1:n_t)')*((eta_t - eta_star_t))+...
        P_t(n_t*i+1:n_t*i+n_t,1:n_t)*P_t(n_t*L_t+n_t+i,:)'); 
    end
end
end
grad_V = grad_V_0 + grad_V_1;
psi = ((grad_V)'*eta_dot_t)/((norm(grad_V,2))*(norm(eta_dot_t,2)));
end