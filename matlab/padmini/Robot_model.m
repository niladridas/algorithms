% Robot Model
function [theta,theta_dot] = Robot_model(M,C,G,tau,old_theta,old_theta_dot,T_s)
global tick;
theta = old_theta;
theta_dot = old_theta_dot;
theta_dot_dot = Inv(M)*(-C -G) + Inv(M)*tau[tick];
theta_dot[tick+1,:] = old_theta_dot[tick,:] + T_s*theta_dot_dot[tick,:];
theta[tick+1,:] = old_theta[tick,:] + T_s*theta_dot[tick,:];
end