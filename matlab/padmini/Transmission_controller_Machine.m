function V_uc_continious_tmp = Transmission_controller_Machine(V_uc_continious,V_uc_discrete) 
V_uc_continious_tmp = V_uc_continious;
% V_uc is discrete
% V_ucd is continious
global T_s;
global tick;
V_uc_continious_tmp[tick+c_i] =  sqrt(T_s)*V_uc_discrete[tick];
return V_uc_continious_tmp;

end