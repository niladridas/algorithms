clc
clear all
%K1 = [90,0,0,0;0,90,0,0;0,0,90,0;0,0,0,90];
%K2 = [90,0,0,0;0,90,0,0;0,0,90,0;0,0,0,90];
A = 1;
w = 0.6;
global U;
U = [];
global count;
count = 0;
global Dx_tmp;
Dx_tmp = [];
global V1;
V1 = [];
global V2;
V2 = [];
fun1 = @(t,x) dynamics1(t,x,@M_full,@C_full,@refsignal,A,w);
[t, x_t] = ode45( fun1,[0 50],[0 0 0 +3.14 0 0 0 0 10 10 10 10 1 1 1 1]) ;