function Dx = dynamics(t,x,Mf,Cf,refsignal,A,w) % Here x = [position;velocity]
global U;
global count;
count = count +1;
M = Mf(x(1:4,1));
C = Cf(x(1:8,1));
[xd,xd_d,xd_dd] = refsignal(t,A,w);
Dx = zeros(16,1);
Dx(1:4,1) = x(5:8,1);
T1 = [x(9),0,0,0;0,x(10),0,0;0,0,x(11),0;0,0,0,x(12)];
T2 = [10,0,0,0;0,10,0,0;0,0,10,0;0,0,0,10];
% if(t <= 10 && t >=5)
%    app_dist = [0;0.003*sin(5*w*t);0;0];
% else 
%    app_dist = [0;0;0;0]; 
% end
app_dist = [0;0;0;0]; 
Dx(5:8) = -(M\C)  + app_dist +((M )\(M*( (M\C) -T1*T2*(x(1:4,1)-xd) + (T1+T2)*(xd_d - x(5:8,1)) + xd_dd)));
global Dx_tmp;
Dx_tmp = [Dx_tmp,-(M\C)];
U = [U,((M*( (M\C) -T1*T2*(x(1:4,1)-xd) + (T1+T2)*(xd_d - x(5:8,1)) + xd_dd)))];
Dist = (-(M\C) +app_dist + M\((M*( (M\C) -T1*T2*(x(1:4,1)-xd) + (T1+T2)*(xd_d - x(5:8,1)) + xd_dd)))) - Dx(5:8);
display(Dist);


if abs(x(1)-xd(1)) >= 0.01
E2 = x(5) - xd_d(1) + x(9).*(x(1)-xd(1));
E1 = (x(1)-xd(1));
%E12 = E2./E1;
Dx(9) = -2.*x(9).*E2 + (2*E2*x(13)*Dist(1)./(E1)) - 1;
else
    Dx(9) = 0;
end

if abs(x(2)-xd(2)) >= 0.01
E2 = x(6) - xd_d(2) + x(10).*(x(2)-xd(2));
E1 = (x(2)-xd(2));
%E12 = E2./E1;
Dx(10) = -2.*x(10).*E2 + (2*E2*x(14)*Dist(2)./(E1)) - 1;
else
    Dx(10) = 0;
end

if abs(x(3)-xd(3)) >= 0.01
E2 = x(7) - xd_d(3) + x(11).*(x(3)-xd(3));
E1 = (x(3)-xd(3));
%E12 = E2./E1;
Dx(11) = -2.*x(11).*E2 + (2*E2*x(15)*Dist(3)./(E1)) - 1;
else
    Dx(11) = 0;
end

if abs(x(4)-xd(4)) >= 0.01
E2 = x(8) - xd_d(4) + x(12).*(x(4)-xd(4));
E1 = (x(4)-xd(4));
%E12 = E2./E1;
Dx(12) = -2.*x(12).*E2 + (2*E2*x(16)*Dist(4)./(E1)) - 1;
else
    Dx(12) = 0;
end

Dx(13) = 0;
Dx(14) = 0;
Dx(15) = 0;
Dx(16) = 0;
