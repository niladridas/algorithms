function [xd,xd_d,xd_dd] = refsignal(t,A,w)
xd = zeros(4,1);
xd_d = zeros(4,1);
xd_dd = zeros(4,1);
xd(2,1) = A*sin(w*t);
xd(4,1) = 3.14;
xd_d(2,1) = A*w*cos(w*t);
xd_dd(2,1) = -A*w*w*sin(w*t);