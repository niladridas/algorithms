function dx = single_pt_attractor(t,tau,x,goal,alpha,beta, alpha_x, W,sigma, y_0,g,c,Mode)
dx = zeros(2,1);
dx(1) = x(2)/tau;
dx(2) = (alpha*(beta*(goal - x(1)) - x(2))+ forcing_function(W,sigma, y_0,g,x(3),c,Mode))/tau;
% dx(1) = (alpha*(goal - x(1))+ forcing_function(W,sigma, y_0,g,x(3),c,Mode))/tau;
% dx(2) = 0;
dx(3) = -alpha_x*x(3);