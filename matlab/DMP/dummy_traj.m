% Dummy Trajectory should give time stamped trajectory
function [Tr,Trd,Trdd] = dummy_traj(t,T,A,B)
    if (A < B)
    error('A must be greater than B')
    end
    sample_size = size(t,1);
    Tr = A*ones(sample_size,1) + B*sin((2*pi/(2*T)).*t + (pi/2)*ones(sample_size,1));
    Trd = B*(2*pi/(2*T))*cos((2*pi/(2*T)).*t + (pi/2)*ones(sample_size,1));
    Trdd = - B*(2*pi/(2*T))*(2*pi/(2*T))*sin((2*pi/(2*T)).*t + (pi/2)*ones(sample_size,1));
end
