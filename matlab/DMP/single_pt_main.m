% Niladri Das
% Dynamic Movement Primitive
% Point Attractor Dynamics
% Discrete Motion (without periodicity)
clear all
clc
%W = [0;0]; % Weightage for each basis function
%sigma  = [1;1]; % Variance vector for each basis function
y_0 = 3; % start position
g = 1; % goal position

tau = 1;
alpha = 4;
beta = 1;
%% Learning the forcing function
% Before learning we need to fix the duration of the trajectory
T = 2; % secs for the whole duration of the motion
A = 2;
B = 1;
No_basis = input('Number of basis u want to use: ');
c_time = linspace(0,T,No_basis)';
alpha_x = log(100)/T;%0.8941;
W = ones(No_basis,1);
c = exp(-alpha_x*c_time);
%c = [0;0]; % Basis centers
% The canonical equation parameter should also be modified alpha_x
t_sample = linspace(0,2,100)';
[Tr,Trd,Trdd] = dummy_traj(t_sample,T,A,B);
F_learn = tau*tau*Trdd - alpha*(beta*(g*ones(size(t_sample,1),1)-Tr) - tau*Trd); 
% F_learn = tau*Trd - alpha*(g*ones(size(t_sample,1),1)-Tr);
% Finding the weight vector
sigma = 0.05*ones(No_basis,1);
S_vector = exp(-alpha_x*t_sample)*(g-y_0);

for j = 1:No_basis
    L_matrix = eye(size(t_sample,1));
for i=1:size(t_sample,1)
    L_matrix(i,i) = exp(-( (exp(-alpha_x*t_sample(i))- c(j))^2)/(2*sigma(j)^2));
end
    W(j) = (S_vector'*L_matrix*F_learn)/(S_vector'*L_matrix*S_vector);
end

% for j = 1:No_basis
%     L_matrix = zeros(size(t_sample,1),1);
% for i=1:size(t_sample,1)
%     L_matrix(i) = exp(-( (exp(-alpha_x*t_sample(i))- c(j))^2)/(2*sigma(j)^2));
% end
%     W(j) = (L_matrix'*F_learn)/(S_vector'*L_matrix);
% end

% for changed goal position

Mode = 'Discrete';
[T,X] = ode45(@(t,x) single_pt_attractor(t,tau,x,g,alpha,beta,alpha_x,W,sigma, y_0,g,c,Mode),[0.80 2],[Tr(40) Trd(1), 1]);
plot(T,X(:,1));
hold on
grid on
%figure
%plot(T,X(:,2));
%grid on
%figure
plot(t_sample,Tr,'r');

% RMS error between the two trajectories
error = 0;
for k = 1:size(T,1)
    error = error + ((X(k,1) - interp1(t_sample,Tr,T(k)))^2); 
end
Rms = sqrt(error/size(T,1))