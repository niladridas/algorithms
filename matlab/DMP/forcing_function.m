% Forcing function
function F = forcing_function(W,sigma, y_0,g,x,c,Mode) 
% Mode := Rhythmic or Discrete
% c(i) := ith basis centre, c is a column vector 
% x := canonical state value
% g := goal value
% y_0 = initial state
% sigma(i) = variance of the i_th basis function, sigma is a column vector
% W(i) = weightage of the i_th basis fucntion, W is a column vector
% Sanity check : The dimension of sigma = No_of_basis = dimension of W
dim_W = size(W,1);
dim_sigma = size(sigma,1);
dim_c = size(c,1);
if dim_W ~= dim_c
    error('Dimension of W not matching with Dimension of basis centers')
end

if dim_sigma ~= dim_c
    error('Dimension of sigma not matching with Dimension of basis centers')
end

if dim_sigma ~= dim_W
    error('Dimension of sigma not matching with Dimension of W')
end

if strcmp(Mode, 'Discrete')
    Psi = exp((-(x*ones(dim_c,1)-c).^2)./(2*sigma.^2));
    F = (sum(Psi.*W)/sum(Psi))*x*(g-y_0);
end

% TODO :
% if Mode = 'Rythmic'
% end

end