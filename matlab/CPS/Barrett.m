function Dx = Barrett(t,x,M_f,C_f,contbarrett,refsgnl,K1,K2)
Dx = zeros(8,1);
M = M_f(x(1:4));
C = C_f(x(1:8));
[xd,xd_d,xd_dd] = refsgnl(t,A,w);
U = contbarrett(x,xd,xd_d,xd_dd,M,C,K1,K2);
Dx(1:4) = x(5:8);
Dx(5:8) = -M\C + M\U;