function U = controlbarrett(x,xd,xd_d,xd_dd,M,C,K1,K2)

U = C + M* (-(eye(4, 4) + K1 * K2)* (x(1:4) - xd) - (K1 + K2) * x(5:8) + (K1 + K2) * xd_d + xd_dd);
end