function [xd,xd_d,xd_dd] = refsignal(t)
xd = zeros(4,1);
xd_d = zeros(4,1);
xd_dd = zeros(4,1);
if(t<2 && t >=0)
   xd(2,1) = 0; 
   xd_d(2,1) = 0;
   xd_dd(2,1) = 0;
end
if(t<5 && t >=2)
    xd(2,1) = (0.8/3)*(t-2);
       xd_d(2,1) = 0.8/3;
   xd_dd(2,1) = 0;
end
if(t<12 && t >=5)
    xd(2,1) = 0.8;
       xd_d(2,1) = 0;
   xd_dd(2,1) = 0;
end
if(t<15 && t >=12)
    xd(2,1) = (0.8/3)*(15-t);
       xd_d(2,1) = -0.8/3;
   xd_dd(2,1) = 0;
end
if(t>=15)
    xd(2,1) = 0;
       xd_d(2,1) = 0;
   xd_dd(2,1) = 0;
end
xd(4,1) = 3.14;
