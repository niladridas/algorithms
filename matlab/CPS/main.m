clear all
clc
K1 = [10,0,0,0;0,10,0,0;0,0,10,0;0,0,0,10];
K2 = [10,0,0,0;0,10,0,0;0,0,10,0;0,0,0,10];
A = 1;
w = 0.6;
fun1 = @(t,x) Barrett(t,x,@M_full,@C_full,@controlbarrett,@refsignal,K1,K2);
[t,x_t] = ode45(fun1,[0 20],[0 0.525 0 3.14 0 0 0 0]);