function dX = func4(t,X)
dX = zeros(2,1);   
dX(1) = -X(1);
dX(2) = -X(1)*cos(X(1))-X(2);
