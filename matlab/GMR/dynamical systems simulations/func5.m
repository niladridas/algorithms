function dX = func5(t,X)
dX = zeros(3,1);   
dX(1) = -X(1)-X(2)+X(3)^2;
dX(2) = X(1)+10*cos(X(2))*X(2)-X(3)^2;
dX(3) = X(1)+2*X(2)-X(3);
