% Learning Non-Linear Multivariate Dynamics of Motion in Robotics
% Manipulation-----Gribovskaya, Khansari and Billard
% System I
clc
clear all
% 6 data-sets have to be generated with different starting points
% Area [-20;30]x[-11;-5]x[-10;2]
% Setting different initial points
I = [-10 -9.5 -5;-2 -5.5 -5 ;0 -10.5 -5; 2 -10.5 -5;5 -5.8 -5;8 -10 -5];

T_M = [];
X_M = [];
X_dot_M = [];
X_dot = [];
for i = 1:6
[T1,X]=ode45(@func5,[0 7],I(i,:));
tmp_X_dot_a = -X(:,1)-X(:,2)+X(:,3).*X(:,3);
tmp_X_dot_b = X(:,1)+10*cos(X(:,2)).*X(:,2);
tmp_X_dot_c = X(:,1)+2*X(:,2)-X(:,3);
X_dot = [tmp_X_dot_a,tmp_X_dot_b,tmp_X_dot_c ];
T_M = cat(2,T_M,T1');
X_M = cat(2,X_M,X');
X_dot_M = cat(2,X_dot_M,X_dot');
end
for i=2:3
scatter(X_M(1,:),X_M(i,:));hold on;
end
scatter(X_M(2,:),X_M(3,:))

% Total concatenated data
Data5 = [X_M;X_dot_M];
% Fitting 12 Gaussians

%% Definition of the number of components used in GMM.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbStates5 = 12;

%% Load a dataset consisting of 6 demonstrations of a 4D signal.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nbVar = size(Data5,1);

%% Training of GMM1 by EM algorithm, initialized by k-means clustering.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Priors1, Mu1, Sigma1] = EM_init_kmeans(Data5, nbStates5);
[Priors1, Mu1, Sigma1] = EM(Data5, Priors1, Mu1, Sigma1);

%% Creation of a mixed model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Priors = Priors1;
Mu = Mu1;
Sigma = Sigma1;

% Use of GMR to retrieve a generalized version of the data and associated
% constraints. A sequence of temporal values is used as input, and the 
% expected distribution is retrieved. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% expData(1,:) = linspace(1, 200, 200);
expData(1,:) = X_M(1,:);
expData(2,:) = X_M(2,:);
expData(3,:) = X_M(3,:);
% col = size(X_M,2);
% [expData(2:nbVar,:), expSigma] = GMR(Priors, Mu, Sigma,  expData(1,:), [1], [2:nbVar]);
[expData(4:nbVar,:),expSigma] = GMR(Priors, Mu, Sigma,  X_M, 1:3, 4:nbVar);

%% Plot of the data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% figure('position',[10,10,1000,800],'name','GMM-GMR-demo3');
%plot 1D
for n=1:nbVar-1
  subplot(6,2,n); hold on;
  plot(Data5(1,:), Data5(n+1,:), 'x', 'markerSize', 4, 'color', [.8 0 0]);
%   plot(Data2(1,:), Data2(n+1,:), 'x', 'markerSize', 4, 'color', [0 .8 0]);
  axis([min(expData(1,:)) max(expData(1,:)) min(expData(n+1,:))-0.02 max(expData(n+1,:))+0.02]);
  xlabel('x1','fontsize',16); ylabel(['x_' num2str(n+1)],'fontsize',16);
end
%plot D
for n=2:nbVar-1
  subplot(6,2,4+n); hold on;
  plot(Data5(2,:), Data5(n+1,:), 'x', 'markerSize', 4, 'color', [.8 0 0]);
%   plot(Data2(1,:), Data2(n+1,:), 'x', 'markerSize', 4, 'color', [0 .8 0]);
  axis([min(expData(2,:)) max(expData(2,:)) min(expData(n+1,:))-0.02 max(expData(n+1,:))+0.02]);
  xlabel('x2','fontsize',16); ylabel(['x_' num2str(n+1)],'fontsize',16);
end
% 
% %plot 2D
for n=3:nbVar-1
  subplot(6,2,7+n); hold on;
  plot(Data5(3,:), Data5(n+1,:), 'x', 'markerSize', 4, 'color', [.8 0 0]);
%   plot(Data2(1,:), Data2(n+1,:), 'x', 'markerSize', 4, 'color', [0 .8 0]);
  axis([min(expData(3,:)) max(expData(3,:)) min(expData(n+1,:))-0.02 max(expData(n+1,:))+0.02]);
  xlabel('x3','fontsize',16); ylabel(['x_' num2str(n+1)],'fontsize',16);
end

% subplot(3*(nbVar-1),2,10); hold on;
% plot(Data5(3,:), Data5(4,:), 'x', 'markerSize', 4, 'color', [.8 0 0]);
% axis([min(expData(3,:))-0.02 max(expData(3,:))+0.02 min(expData(4,:))-0.02 max(expData(4,:))+0.02]);
% xlabel('x_3','fontsize',16); ylabel('x_4','fontsize',16);
% subplot(3*(nbVar-1),2,11); hold on;
% plot(Data5(3,:), Data5(5,:), 'x', 'markerSize', 4, 'color', [0 .8 0]);
% axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(4,:))-0.02 max(expData(4,:))+0.02]);
% xlabel('x_2','fontsize',16); ylabel('x_4','fontsize',16);

% Plot of the GMM encoding results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot 1D
figure 

for n=1:nbVar-1
  subplot(6,2,n); hold on;
  plotGMM(Mu1([1,n+1],:), Sigma1([1,n+1],[1,n+1],:), [.8 0 0], 1);
%   plotGMM(Mu2([1,n+1],:), Sigma2([1,n+1],[1,n+1],:), [0 .8 0], 1);
  axis([min(expData(1,:)) max(expData(1,:)) min(expData(n+1,:))-0.02 max(expData(n+1,:))+0.02]);
  xlabel('x1','fontsize',16); ylabel(['x_' num2str(n+1)],'fontsize',16);
end
%plot D
for n=2:nbVar-1
  subplot(6,2,4+n); hold on;
  plotGMM(Mu1([2,n+1],:), Sigma1([2,n+1],[2,n+1],:), [.8 0 0], 1);
%   plotGMM(Mu2([1,n+1],:), Sigma2([1,n+1],[1,n+1],:), [0 .8 0], 1);
  axis([min(expData(2,:)) max(expData(2,:)) min(expData(n+1,:))-0.02 max(expData(n+1,:))+0.02]);
  xlabel('x2','fontsize',16); ylabel(['x_' num2str(n+1)],'fontsize',16);
end
%plot 2D
for n=3:nbVar-1
  subplot(6,2,7+n); hold on;
  plotGMM(Mu1([3,n+1],:), Sigma1([3,n+1],[3,n+1],:), [.8 0 0], 1);
%   plotGMM(Mu2([1,n+1],:), Sigma2([1,n+1],[1,n+1],:), [0 .8 0], 1);
  axis([min(expData(3,:)) max(expData(3,:)) min(expData(n+1,:))-0.02 max(expData(n+1,:))+0.02]);
  xlabel('x3','fontsize',16); ylabel(['x_' num2str(n+1)],'fontsize',16);
end
% % plot2D
% % subplot(3*(nbVar-1),2,13); hold on;
% % plotGMM(Mu1([2,3],:), Sigma1([2,3],[2,3],:), [.8 0 0], 1);
% % % plotGMM(Mu2([2,3],:), Sigma2([2,3],[2,3],:), [0 .8 0], 1);
% % axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(3,:))-0.02 max(expData(3,:))+0.02]);
% % xlabel('x_2','fontsize',16); ylabel('x_3','fontsize',16);
% % 
% %subplot(3*(nbVar-1),2,14); hold on;
% %plotGMM(Mu1([2,4],:), Sigma1([2,4],[2,4],:), [.8 0 0], 1);
% % plotGMM(Mu2([2,3],:), Sigma2([2,3],[2,3],:), [0 .8 0], 1);
% %axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(4,:))-0.02 max(expData(4,:))+0.02]);
% %xlabel('x_2','fontsize',16); ylabel('x_4','fontsize',16);
% 
%% Plot of the GMR regression results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot 1D
figure

for n=1:3
  subplot(5,2,n); hold on;
  plotGMM(expData([1,n+3],:), expSigma(n,n,:), [0 0 .8], 2);
  axis([min(expData(1,:)) max(expData(1,:)) min(expData(n+3,:))-0.02 max(expData(n+3,:))+0.02]);
  xlabel('x1','fontsize',16); ylabel(['x_' num2str(n+3)],'fontsize',16);
end
%plot D
for n=1:3
  subplot(5,2,3+n); hold on;
  plotGMM(expData([2,n+3],:), expSigma(n,n,:), [0 0 .8], 2);
  axis([min(expData(2,:)) max(expData(2,:)) min(expData(n+3,:))-0.02 max(expData(n+3,:))+0.02]);
  xlabel('x2','fontsize',16); ylabel(['x_' num2str(n+3)],'fontsize',16);
end
%plot 2D
for n=1:3
  subplot(5,2,6+n); hold on;
  plotGMM(expData([3,n+3],:), expSigma(n,n,:), [0 0 .8], 2);
  axis([min(expData(3,:)) max(expData(3,:)) min(expData(n+3,:))-0.02 max(expData(n+3,:))+0.02]);
  xlabel('x3','fontsize',16); ylabel(['x_' num2str(n+3)],'fontsize',16);
end
% %plot 2D
% subplot(3*(nbVar-1),2,19); hold on;
% plotGMM(expData([2,3],:), expSigma(1,1,:), [0 0 .8], 2);
% axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(3,:))-0.02 max(expData(3,:))+0.02]);
% xlabel('x_2','fontsize',16); ylabel('x_3','fontsize',16);
% % 
% subplot(3*(nbVar-1),2,15); hold on;
% plotGMM(expData([2,4],:), expSigma(2,2,:), [0 0 .8], 2);
% axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(4,:))-0.02 max(expData(4,:))+0.02]);
% xlabel('x_2','fontsize',16); ylabel('x_4','fontsize',16);
% pause
% close all;
% % 
% % 
% % 
% % 
