function dX = func3(t,X)
dX = zeros(2,1);   
dX(1) = -X(2);
dX(2) = X(1)-X(1)^3-X(2);
