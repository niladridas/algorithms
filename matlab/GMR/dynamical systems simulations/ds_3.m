% Learning Non-Linear Multivariate Dynamics of Motion in Robotics
% Manipulation-----Gribovskaya, Khansari and Billard
% System I
clc
clear all
% 6 data-sets have to be generated with different starting points
% Area [-1.5;1]x[-1.5;0.5]
% Setting different initial points
% I = [-1.5 0.5;-1.25 0.25; -1 -1.5 ; 0.75 -1.25 ;0.5 -1; 0 0.5];
I = [-1 -1;-0.75 -0.4;-0.4 -1.2;-0.5 -0.4;-0.25 -0.75;-0.5 -0.75];

T_M = [];
X_M = [];
X_dot_M = [];
X_dot = [];
for i = 1:6
[T1,X]=ode45(@func3,[0 3],I(i,:));

% TEMPORARY COMMAND
%file_ident = strcat('/home/niladriisl/Dropbox/RA_work/Imitation Learning/new_codes/Data_file/a',int2str(i),'.txt');

tmp_X_dot_a = -X(:,2);
tmp_X_dot_b = X(:,1)-X(:,1).*X(:,1).*X(:,1)-X(:,2);
X_dot = [tmp_X_dot_a,tmp_X_dot_b];
temp_variables = [X,X_dot];
%dlmwrite(file_ident,temp_variables);

T_M = cat(2,T_M,T1');
X_M = cat(2,X_M,X'); % The state values 
X_dot_M = cat(2,X_dot_M,X_dot'); % Derivative of the state values
end

% scatter(X_M(1,:),X_M(2,:))

% Total concatenated data
Data3 = [X_M;X_dot_M];
% Fitting 7 Gaussians

%% Definition of the number of components used in GMM.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbStates3 = 6;

%% Load a dataset consisting of 6 demonstrations of a 4D signal.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nbVar = size(Data3,1);

%% Training of GMM1 by EM algorithm, initialized by k-means clustering.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Priors1, Mu1, Sigma1] = EM_init_kmeans(Data3, nbStates3);
[Priors1, Mu1, Sigma1] = EM(Data3, Priors1, Mu1, Sigma1);

%% Creation of a mixed model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Priors = Priors1;
Mu = Mu1;
Sigma = Sigma1;

% Use of GMR to retrieve a generalized version of the data and associated
% constraints. A sequence of temporal values is used as input, and the 
% expected distribution is retrieved. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% expData(1,:) = linspace(1, 200, 200);
expData(1,:) = X_M(1,:);
expData(2,:) = X_M(2,:);
% col = size(X_M,2);
% [expData(2:nbVar,:), expSigma] = GMR(Priors, Mu, Sigma,  expData(1,:), [1], [2:nbVar]);
[expData(3:nbVar,:),expSigma] = GMR(Priors, Mu, Sigma,  X_M, 1:2, 3:nbVar);

scatter(X_M(1,:),X_dot_M(1,:))
xlabel('x_1')
ylabel('$\dot{x_1}$ actual','interpreter','latex')
title('ACTUAL DYNAMICS ')
grid on
figure
scatter(X_M(2,:),X_dot_M(2,:))
xlabel('x_2')
ylabel('$\dot{x_2}$ actual','interpreter','latex')
title('ACTUAL DYNAMICS ')
grid on
figure
scatter(X_M(1,:),expData(3,:),'r')
xlabel('x_1')
ylabel('$\dot{x_1}$  reproduced','interpreter','latex')
title('REPRODUCED DYNAMICS')
grid on
figure
scatter(X_M(2,:),expData(4,:),'r')
xlabel('x_2')
ylabel('$\dot{x_2}$  reproduced','interpreter','latex')
title('REPRODUCED DYNAMICS ')
grid on

% %% Plot of the data
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % figure('position',[10,10,1000,800],'name','GMM-GMR-demo3');
% %plot 1D
% for n=1:nbVar-1
%   subplot(3*(nbVar-1),2,n); hold on;
%   plot(Data3(1,:), Data3(n+1,:), 'x', 'markerSize', 4, 'color', [.8 0 0]);
% %   plot(Data2(1,:), Data2(n+1,:), 'x', 'markerSize', 4, 'color', [0 .8 0]);
%  axis([min(expData(1,:)) max(expData(1,:)) min(expData(n+1,:))-0.02 max(expData(n+1,:))+0.02]);
%   xlabel('x1','fontsize',16); ylabel(['x_' num2str(n+1)],'fontsize',16);
% end
% %plot 2D
% subplot(3*(nbVar-1),2,(nbVar-1)+1); hold on;
% plot(Data3(2,:), Data3(3,:), 'x', 'markerSize', 4, 'color', [.8 0 0]);
% axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(3,:))-0.02 max(expData(3,:))+0.02]);
% xlabel('x_2','fontsize',16); ylabel('x_3','fontsize',16);
% subplot(3*(nbVar-1),2,(nbVar-1)+2); hold on;
% plot(Data3(2,:), Data3(4,:), 'x', 'markerSize', 4, 'color', [0 .8 0]);
% axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(4,:))-0.02 max(expData(4,:))+0.02]);
% xlabel('x_2','fontsize',16); ylabel('x_4','fontsize',16);
% 
% %% Plot of the GMM encoding results
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %plot 1D
% for n=1:nbVar-1
%   subplot(3*(nbVar-1),2,5+n); hold on;
%   plotGMM(Mu1([1,n+1],:), Sigma1([1,n+1],[1,n+1],:), [.8 0 0], 1);
% %   plotGMM(Mu2([1,n+1],:), Sigma2([1,n+1],[1,n+1],:), [0 .8 0], 1);
%   axis([min(expData(1,:)) max(expData(1,:)) min(expData(n+1,:))-0.02 max(expData(n+1,:))+0.02]);
%   xlabel('x1','fontsize',16); ylabel(['x_' num2str(n+1)],'fontsize',16);
% end
% %plot 2D
% subplot(3*(nbVar-1),2,9); hold on;
% plotGMM(Mu1([2,3],:), Sigma1([2,3],[2,3],:), [.8 0 0], 1);
% % plotGMM(Mu2([2,3],:), Sigma2([2,3],[2,3],:), [0 .8 0], 1);
% axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(3,:))-0.02 max(expData(3,:))+0.02]);
% xlabel('x_2','fontsize',16); ylabel('x_3','fontsize',16);
% 
% subplot(3*(nbVar-1),2,10); hold on;
% plotGMM(Mu1([2,4],:), Sigma1([2,4],[2,4],:), [.8 0 0], 1);
% % plotGMM(Mu2([2,3],:), Sigma2([2,3],[2,3],:), [0 .8 0], 1);
% axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(4,:))-0.02 max(expData(4,:))+0.02]);
% xlabel('x_2','fontsize',16); ylabel('x_4','fontsize',16);
% 
% %% Plot of the GMR regression results
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %plot 1D
% for n=1:2
%   subplot(3*(nbVar-1),2,10+n); hold on;
%   plotGMM(expData([1,n+2],:), expSigma(n,n,:), [0 0 .8], 3);
%   axis([min(expData(1,:)) max(expData(1,:)) min(expData(n+2,:))-0.02 max(expData(n+2,:))+0.02]);
%   xlabel('x1','fontsize',16); ylabel(['x_' num2str(n+2)],'fontsize',16);
% end
% %plot 2D
% subplot(3*(nbVar-1),2,14); hold on;
% plotGMM(expData([2,3],:), expSigma(1,1,:), [0 0 .8], 2);
% axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(3,:))-0.02 max(expData(3,:))+0.02]);
% xlabel('x_2','fontsize',16); ylabel('x_3','fontsize',16);
% % 
% subplot(3*(nbVar-1),2,15); hold on;
% plotGMM(expData([2,4],:), expSigma(2,2,:), [0 0 .8], 2);
% axis([min(expData(2,:))-0.02 max(expData(2,:))+0.02 min(expData(4,:))-0.02 max(expData(4,:))+0.02]);
% xlabel('x_2','fontsize',16); ylabel('x_4','fontsize',16);
% pause
% close all;




