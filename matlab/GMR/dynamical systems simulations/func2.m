function dx = func2(t,x)
dx = zeros(2,1);   
dx(1) = 700-2*x(1)+200*x(2)*exp((25*x(1)-10^4)/x(1));
dx(2) = 1-x(2)-x(2)*exp((25*x(1)-10^4)/x(1));
