% % experimental results
% figure
% plot(all_data(:,5))
% hold on;plot(expData_third_final(1,:),'r')
% legend('Actual veclocity ','Reproduced veclocity')
% ylabel('X_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('Third exp: Existing Method ')
% grid on;
% 
% figure
% plot(all_data(:,6))
% hold on;plot(expData_third_final(2,:),'r')
% legend('Actual veclocity','Reproduced veclocity')
% ylabel('Y_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('Third exp: Existing Method')
% grid on;
% 
% figure
% plot(all_data(:,7))
% hold on;plot(expData_third_final(3,:),'r')
% legend('Actual veclocity','Reproduced veclocity')
% ylabel('Z_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('Third exp: Existing Method')
% grid on;

% figure
% plot(validating_data(7,:))
% hold on;plot(expData_validating(1,:),'r')
% legend('Actual veclocity in x','Reproduced veclocity ')
% ylabel('X_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('VALIDATING (second exp): PROPOSED METHOD')
% grid on;
% 
% figure
% plot(validating_data(8,:))
% hold on;plot(expData_validating(2,:),'r')
% legend('Actual veclocity ','Reproduced veclocity')
% ylabel('Y_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('VALIDATING (second exp): PROPOSED METHOD')
% grid on;
% 
% figure
% plot(validating_data(9,:))
% hold on;plot(expData_validating(3,:),'r')
% legend('Actual veclocity ','Reproduced veclocity ')
% ylabel('Z_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('VALIDATING (second exp): PROPOSED METHOD')
% grid on;
% 
New_X_test_error = sqrt(sum((training_data(7,:) - expData_training(1,:)).^2)/size(expData_training(1,:),2))
New_Y_test_error = sqrt(sum((training_data(8,:) - expData_training(2,:)).^2)/size(expData_training(2,:),2))
New_Z_test_error = sqrt(sum((training_data(9,:) - expData_training(3,:)).^2)/size(expData_training(3,:),2))
% 
% New_X_val_error = sqrt(sum((validating_data(7,:) - expData_validating(1,:)).^2)/size(expData_validating(1,:),2))
% New_Y_val_error = sqrt(sum((validating_data(8,:) - expData_validating(2,:)).^2)/size(expData_validating(2,:),2))
% New_Z_val_error = sqrt(sum((validating_data(9,:) - expData_validating(3,:)).^2)/size(expData_validating(3,:),2))


% figure
% plot(training_data_comp(4,:))
% hold on;plot(expData_training_comp(1,:),'r')
% legend('Actual veclocity in x direction','Reproduced veclocity in x direction')
% ylabel('X_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('TESTING (first exp): Existing Method ')
% grid on;
% 
% figure
% plot(training_data_comp(5,:))
% hold on;plot(expData_training_comp(2,:),'r')
% legend('Actual veclocity in y direction','Reproduced veclocity in y direction')
% ylabel('Y_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('TESTING (first exp): Existing Method ')
% grid on;
% 
% figure
% plot(training_data_comp(6,:))
% hold on;plot(expData_training_comp(3,:),'r')
% legend('Actual veclocity in z direction','Reproduced veclocity in z direction')
% ylabel('Z_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('TESTING (first exp): Existing Method ')
% grid on;
% 
% figure
% plot(validating_data_comp(4,:))
% hold on;plot(expData_validating_comp(1,:),'r')
% legend('Actual veclocity in x direction','Reproduced veclocity in x direction')
% ylabel('X_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('VALIDATION (first exp): Existing Method')
% grid on;
% 
% figure
% plot(validating_data_comp(5,:))
% hold on;plot(expData_validating_comp(2,:),'r')
% legend('Actual veclocity in y direction','Reproduced veclocity in y direction')
% ylabel('Y_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('VALIDATION (first exp): Existing Method ')
% grid on;
% 
% figure
% plot(validating_data_comp(6,:))
% hold on;plot(expData_validating_comp(3,:),'r')
% legend('Actual veclocity in z direction','Reproduced veclocity in z direction')
% ylabel('Z_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('VALIDATION (first exp): Existing Method ')
% % grid on;
% 
% Old_X_test_error = sqrt(sum((training_data_comp(4,:) - expData_training_comp(1,:)).^2)/size(expData_training_comp(1,:),2))
% Old_Y_test_error = sqrt(sum((training_data_comp(5,:) - expData_training_comp(2,:)).^2)/size(expData_training_comp(2,:),2))
% Old_Z_test_error = sqrt(sum((training_data_comp(6,:) - expData_training_comp(3,:)).^2)/size(expData_training_comp(3,:),2))
% 
% Old_X_val_error = sqrt(sum((validating_data_comp(4,:) - expData_validating_comp(1,:)).^2)/size(expData_validating_comp(1,:),2))
% Old_Y_val_error = sqrt(sum((validating_data_comp(5,:) - expData_validating_comp(2,:)).^2)/size(expData_validating_comp(2,:),2))
% Old_Z_val_error = sqrt(sum((validating_data_comp(6,:) - expData_validating_comp(3,:)).^2)/size(expData_validating_comp(3,:),2))

% figure
% plot( training_data_comp(4,:))
% hold on;plot(expData_final(1,:),'r')
% legend('Actual veclocity in x direction','Reproduced veclocity in x direction')
% ylabel('X_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('Second exp: Existing Method')
% grid on;
% 
% figure
% plot( training_data_comp(5,:))
% hold on;plot(expData_final(2,:),'r')
% legend('Actual veclocity in y direction','Reproduced veclocity in y direction')
% ylabel('Y_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('Second exp: Existing Method')
% grid on;
% 
% figure
% plot( training_data_comp(6,:))
% hold on;plot(expData_final(3,:),'r')
% legend('Actual veclocity in z direction','Reproduced veclocity in z direction')
% ylabel('Z_{vel} (m/sec)')
% xlabel('All Demonstrations for 9 gaussians')
% title('Second exp: Existing Method')
% grid on;

% Old_X_test_error_2 = sqrt(sum((  all_data(:,5)' - expData_third_final(1,:)).^2)/size(expData_third_final(1,:),2))
% Old_Y_test_error_2 = sqrt(sum((  all_data(:,6)' - expData_third_final(2,:)).^2)/size(expData_third_final(2,:),2))
% Old_Z_test_error_2 = sqrt(sum((  all_data(:,7)' - expData_third_final(3,:)).^2)/size(expData_third_final(3,:),2))
% final_demo1 = [];
% for i = 1:2:20
%     final_demo1 = [final_demo1;temp_cell{i}];
% end
% scatter3(final_demo1(1:50:end,1),final_demo1(1:50:end,2),final_demo1(1:50:end,3),'r')
% zlabel('Z (m)')
% ylabel('Y (m)')
% xlabel('X (m)')
% title('Third Demonstration')
% grid on;

% [expData_third_final(1:3,:),expSigma_third] = GMR(Priors_comp, Mu_comp, Sigma_comp,  all_data(:,2:4)'+ repmat([0.5532;-0.1717;-0.2023]-[0.7141;0.0952;-0.2135],1,size(all_data,1)), 1:3, 4:6);
