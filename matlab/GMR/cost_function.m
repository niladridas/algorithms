% Weighted sum of Asymmetric Quadratic Functions
% The values of eta and eta_dot are arranged as follows :
% eta and eta_dot for a single observation are arranged one below the other
% eta and eta_dot for several observations are concatenated side by side
% We assume the such arrangement is already available in the workspace. 
% 
% Here variables are the P matrices and the mean vector
% The number of asymmetric components is choosen by the user

function cost = cost_function(P)
global eta; 
global eta_dot;
global eta_star;
global eta_col;
global w_bar;
global L;
% Here P is a matrix 
% The size of P tells us the number of components of the asymmetric term

[row_P, col_P] = size(P);
n = col_P;

cost = 0;
for i = 1:eta_col
    cost = ((1+w_bar)/2)*sign(cost_psi(P,eta(:,i),eta_dot(:,i),eta_star,L,n))*...
        (cost_psi(P,eta(:,i),eta_dot(:,i),eta_star,L,n))^2 + ...
        ((1-w_bar)/2)*(cost_psi(P,eta(:,i),eta_dot(:,i),eta_star,L,n))^2;
end




