% 4DOF joint co-ordinate to x y z co-ordinates


function XYZ = jp2tp_4DOF(theta4)
global a ;
global alpha;
global d;
% theta4 is a 4 x no_of_data matrix
% XYZ is a 3 x no_of_data matrix
XYZ = zeros(3,size(theta4,2));
for j = 1:size(theta4,2)
    Tmp_trans = eye(4);
    for i = 1:4
        Tmp_trans = Tmp_trans*frame_transformation(a(i),alpha(i),d(i),theta4(i,j));
    end
    Tmp_trans = Tmp_trans*frame_transformation(a(5),alpha(5),d(5),0);
    XYZ(:,j) = Tmp_trans(1:3,4);
end
end