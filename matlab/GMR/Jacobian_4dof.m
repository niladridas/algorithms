% Finding out the jacobian matrix for 4 dof barrett wam
clc
clear all
syms q1 q2 q3 q4 real
q = [q1 q2 q3 q4 0];
a = [0 0 0.045 -0.045 0];
alpha = [-pi/2 pi/2 -pi/2 pi/2 0 ];
d = [0 0 0.55 0 (0.35+0.12)]; % 0.12 for the extra part on the hand
tmp_frame = eye(4);
for i = 1:5
    tmp_frame = tmp_frame*frame_transformation(a(i),alpha(i),d(i),q(i));
end
position = vpa(tmp_frame(1:3,4),2);
Jacobian = [diff(position(1),q1) diff(position(1),q2) diff(position(1),q3) diff(position(1),q4);
            diff(position(2),q1) diff(position(2),q2) diff(position(2),q3) diff(position(2),q4);
            diff(position(3),q1) diff(position(3),q2) diff(position(3),q3) diff(position(3),q4)];
Jacobian = vpa(Jacobian,2);