% Expectation Maximization
function [Priors, Mu, Sigma] = ExpMax(Data, Priors0,Mu0,Sigma0)
% Data is of the format Num_states x Num_data
% Priors0 is row vector of 1xK where K is the number of gaussians
% Mu0 is a Num_states x K matrix
% Sigma0 is a Num_states x Num_states x k multidimensional matrix
loglik_threshold = 1e-10;
[Num_states, Num_data] = size(Data);
Num_gauss = size(Sigma0,3);
loglik_old = -realmax;
nbStep = 0;

Mu = Mu0;
Sigma = Sigma0;
Priors = Priors0;

while 1
% % E step
    for i = 1:Num_gauss
        % We need to calculating the posterior probability 
        % P(i|x) for all the data points
        Pxi(:,i) = gaussPdf_nilu(Data,Mu(:,i),Sigma(:,:,i));
    end
    Pix_tmp = repmat(Priors,[Num_data 1]).*Pxi;
    Pix = Pix_tmp./repmat(sum(Pix_tmp,2),[1 Num_gauss]);
    E = sum(Pix);
% % M step
for i = 1:Num_gauss
    % Prior update
    Priors(i) = E(i)/Num_data;
    % Update the mean
    Mu(:,i) = Data*Pix(:,i) / E(i);
    % Update the covariance matrix
    Data_tmp1 = Data - repmat(Mu(:,i),1,Num_data);
    Sigma(:,:,i) = (repmat(Pix(:,i)',Num_states,1).*Data_tmp1*Data_tmp1')/ E(i);
    Sigma(:,:,i) = Sigma(:,:,i) + 1E-5.*diag(ones(Num_states,1));
end
for i=1:Num_gauss
    %Compute the new probability p(x|i)
    Pxi(:,i) = gaussPdf_nilu(Data, Mu(:,i), Sigma(:,:,i));
end
F = Pxi*Priors';
F(find(F<realmin)) = realmin;
loglik = mean(log(F));
if abs((loglik/loglik_old)-1) < loglik_threshold
    break;
end
  loglik_old = loglik;
  nbStep = nbStep+1;
end%% Add a tiny variance to avoid numerical instability
for i=1:Num_gauss
  Sigma(:,:,i) = Sigma(:,:,i) + 1E-5.*diag(ones(Num_states,1));
end

end