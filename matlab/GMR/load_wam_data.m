%% ----------------------------------------------------------------------
% TODO :Take as a hidden variable the horizontal , distance of the equilibrium point, it's direction and also the altitude. 
% But for now we are going to experiment on the end effector x y x positions only
% clc
% clear all
% Parent_Data_PP = struct;
% oldFolder_PP = cd('/home/nilxwam/imit_data/'); % For point to point motion
% files_PP = dir('*.txt');
% for i=1:length(files_PP)
%     display(files_PP(i).name);
%     Parent_Data_PP(i).index = i; % Index of the demonstration number
%     Parent_Data_PP(i).matrix = load(files_PP(i).name,' -ascii');
%     Parent_Data_PP(i).size = size(Parent_Data_PP(i).matrix);
%     Parent_Data_PP(i).start_pos = Parent_Data_PP(i).matrix(end,:);% Equilibrium point of this demonstration  
% end
% cd(oldFolder_PP)
% save('Parent_Data_PP.mat', 'Parent_Data_PP');
% 
% Parent_Data_Hit = struct;
% oldFolder_Hit = cd('/home/nilxwam/tennsi_data/'); % For point to point motion
% files_Hit = dir('*.txt');
% for i=1:length(files_Hit)
%     display(files_Hit(i).name);
%     Parent_Data_Hit(i).index = i; % Index of the demonstration number
%     Parent_Data_Hit(i).matrix = load(files_Hit(i).name,' -ascii');
%     Parent_Data_Hit(i).size = size(Parent_Data_Hit(i).matrix);
%     Parent_Data_Hit(i).start_pos = Parent_Data_Hit(i).matrix(end,:);% Equilibrium point of this demonstration  
% end
% cd(oldFolder_Hit)
% save('Parent_Data_PP.mat', 'Parent_Data_PP');
% % For now we wont be using the hitting data

% % Here we will be doing a kind of model selection
% Say 30 percent of the demonstration will be used for validation purpose
% First find out how many number of demonstrations are there
% Hence join the 
clc
clear all

%% Some global variables

global eta_star_comp ;
global eta_col_comp;
global w_bar_comp;
global eta_row_comp;
global eta_comp ;
global eta_dot_comp ;

% DH parameter for 4 dof robot
global a ;
a = [0 0 0.045 -0.045 0];
global alpha;
alpha = [-pi/2 pi/2 -pi/2 pi/2 0 ];
global d;
d = [0 0 0.55 0 0.35];


%%

Parent_Data_PP = struct;
oldFolder_PP = cd('/home/nilxwam/imit_data/demo3/'); % For point to point motion
files_PP = dir('*.txt');
Parent_PP = [];
for i=1:length(files_PP)
    display(files_PP(i).name);
    Parent_Data_PP(i).index = i; % Index of the demonstration number
    Parent_Data_PP(i).matrix = load(files_PP(i).name,' -ascii');
    Parent_Data_PP(i).size = size(Parent_Data_PP(i).matrix);
    Parent_Data_PP(i).start_pos = Parent_Data_PP(i).matrix(end,:);% Equilibrium point of this demonstration  
    % Join the matrices into one matrix
    Parent_PP = [Parent_PP;Parent_Data_PP(i).matrix];
end
cd(oldFolder_PP)
save('Parent_PP.mat', 'Parent_PP');

load('Parent_PP.mat'); % All data is here
all_data_tmp = Parent_PP;

%% Preprocess whole of the data to transform from joint co-ordinates to 
% position co-ordinates
all_data = [all_data_tmp(:,1),jp2tp_4DOF(all_data_tmp(:,2:5)')',jv2tv_4DOF(all_data_tmp(:,2:5)', all_data_tmp(:,6:9)')'];

%%


% Data format must be (index,STATES, STATE_DERIVATIVES)
[m,n]=size(all_data); % Determine size of data
No_states = (n-1)/2; % Determine the order of the system
p=zeros(2,1); % save index of consecutive break points denoting change in demonstrations
p(1,1) = 0;
count = 0;
temp_cell={};% define cell to store diff demonstrations
for i=1:m-1 
   if all_data(i+1,1) < all_data(i,1)
       count = count +1;
        p(2,1)=i;
        temp_cell{count,1} = all_data(p(1,1)+1:p(2,1),2:end); 
        p(1,1)=i;
   end
    if i == m-1
          temp_cell{count+1,1} = all_data(p(1,1)+1:m,2:end); 
          end
    i=i+1;
end

total_no_demo = size(temp_cell,1);

fprintf('Number of demonstrations %i\n', total_no_demo);

% 30 percentage of the data are taken for validation
valid_data_per = input('Percentage of data to be used for validation: ');
valid_data_per = round(valid_data_per*total_no_demo/100);

%combos = combntns(1:total_no_demo,valid_data_per); % Giving the index of the 
% demonstrations to be taken for validation
% This function is returning number of combinations exceeding computer
% limit
% For now 
combos = [3 5 20];
%valid_demo_no = input('enter which demonstration to be taken for validation: ');

%% The outermost loop
% The global parameters to be saved
% State derivative prediction error
%combo_size = size(combos,1);
% DEBUG START# 
combo_size = 1;
% DEBUG END#
for j = 1:combo_size
    training_data = [];
    training_data_comp = [];
    validating_data = [];
    validating_data_comp = [];
    for i=1:total_no_demo
        if isempty(find(i==combos(j,:))) 
            % Modifying the training data
            % Adding a new set of colums
            A1_tmp = temp_cell{i};
            num_data = size(A1_tmp,1);
            B1_tmp = A1_tmp(:,1:No_states);
            C1_tmp = A1_tmp(:,No_states+1:end);
            D1_tmp = repmat(A1_tmp(end,1:No_states),[num_data,1]);
            E1_tmp = [B1_tmp,D1_tmp, C1_tmp];%, zeros(num_data,No_states)];
            training_data = cat(2,training_data,E1_tmp');
            training_data_comp =  cat(2,training_data_comp,temp_cell{i}');
        else
            A1_tmp = temp_cell{i};
            num_data = size(A1_tmp,1);
            B1_tmp = A1_tmp(:,1:No_states);
            C1_tmp = A1_tmp(:,No_states+1:end);
            D1_tmp = repmat(A1_tmp(end,1:No_states),[num_data,1]);
            E1_tmp = [B1_tmp,D1_tmp, C1_tmp];%, zeros(num_data,No_states)];
            validating_data =  cat(2,validating_data,E1_tmp');
            validating_data_comp = cat(2, validating_data_comp,temp_cell{i}');
        end
    end
save('training_data.mat','training_data');
save('training_data_comp.mat','training_data_comp');
save('validating_data.mat','validating_data');
save('validating_data_comp.mat','validating_data_comp');

temp_ip_op = training_data'; % (STATE+STATE_DERIVATIVE)
temp_ip_op_comp = training_data_comp'; % (STATE+STATE_DERIVATIVE)

% First calculate the equilibrium point
% This value of x,y,z co-ordinate should come as an external input
% But for now we are just assuming it to be the average value of the end point from all the demonstrations
 
end_position_vector_comp = [0;0;0];
 
for i = 1:total_no_demo
 	temp_demo_matrix_comp = temp_cell{i};
 	end_position_vector_comp = (temp_demo_matrix_comp(end,1:3)')/total_no_demo + end_position_vector_comp;
end
Data_operate = training_data; 
% The number of Gaussian components to be used
nogaussians = input('Total number of gaussians to be used in variable equilibrium points case');
[noVar,noData] = size(Data_operate);
[Data_id,Centres] = kmeans(Data_operate', nogaussians);
Mu_k = Centres';
for i=1:nogaussians
   idtmp = find(Data_id==i);
   Priors_k(i) = length(idtmp);
   Sigma_k(:,:,i) = cov([Data_operate(:,idtmp) Data_operate(:,idtmp)]');
   %Add a tiny variance to avoid numerical instability
   Sigma_k(:,:,i) = Sigma_k(:,:,i) + 1E-5.*diag(ones(noVar,1));
 end
Priors_k = Priors_k ./ sum(Priors_k);
[Priors1, Mu1, Sigma1] = ExpMax(Data_operate, Priors_k, Mu_k, Sigma_k);
 
% % Creation of a mixed model
Priors = Priors1;
Mu = Mu1;
Sigma = Sigma1;
% calculating trainig error and validation error
[expData_training(1:3,:),expSigma_training] = GMR(Priors, Mu, Sigma,  training_data(1:6,:), 1:6, 7:9);
[expData_validating(1:3,:),expSigma_validating] = GMR(Priors, Mu, Sigma,  validating_data(1:6,:), 1:6, 7:9);


if(input('Does all the demonstrations have the same equilibrium point. Enter 1 if yes or 0 otherwise')==1)
%% This part of the code is exclusively for the _comp part which is a replication of the paper
% Finding out the V for the particular demonstration
% Finding out V 
 
%global eta_star_comp ;
%global eta_col_comp;
%global w_bar_comp;
%global eta_row_comp;
%global eta_comp ;
%global eta_dot_comp ;

eta_star_comp = end_position_vector_comp;
save('eta_star_comp.mat','eta_star_comp');
eta_comp = training_data_comp(1:3,:);
eta_dot_comp = training_data_comp(4:6,:);
 
[eta_row_comp,eta_col_comp] = size(eta_comp);
 
% w_bar_comp = 0.1;% %---------------------------------------------------------
% 
% fprintf('The value of the weight factor set is %d\n', w_bar_comp);
% display('It is assumed that there is no assymetric component');
% 
% P0_comp = eye(eta_row_comp,eta_row_comp);
%  
% P_comp = fmincon(@cost_function,P0_comp,[],[],[],[],[],[],@positive_def);

%% Now comes the main gaussian training part
% There are two parts: One for the varying equilibrium points
% Another for the fixed equilibrium point

%% First for the varying equilibrium point
% Data_operate = training_data; 
% % The number of Gaussian components to be used
% nogaussians = input('Total number of gaussians to be used in variable equilibrium points case');
% [noVar,noData] = size(Data_operate);
% [Data_id,Centres] = kmeans(Data_operate', nogaussians);
% Mu_k = Centres';
% for i=1:nogaussians
%    idtmp = find(Data_id==i);
%    Priors_k(i) = length(idtmp);
%    Sigma_k(:,:,i) = cov([Data_operate(:,idtmp) Data_operate(:,idtmp)]');
%    %Add a tiny variance to avoid numerical instability
%    Sigma_k(:,:,i) = Sigma_k(:,:,i) + 1E-5.*diag(ones(noVar,1));
%  end
% Priors_k = Priors_k ./ sum(Priors_k);
% [Priors1, Mu1, Sigma1] = ExpMax(Data_operate, Priors_k, Mu_k, Sigma_k);
%  
% % % Creation of a mixed model
% Priors = Priors1;
% Mu = Mu1;
% Sigma = Sigma1;
% % calculating trainig error and validation error
% [expData_training(1:3,:),expSigma_training] = GMR(Priors, Mu, Sigma,  training_data(1:6,:), 1:6, 7:9);
% [expData_validating(1:3,:),expSigma_validating] = GMR(Priors, Mu, Sigma,  validating_data(1:6,:), 1:6, 7:9);
% 




%% Second for the fixed equilibrium point
Data_operate_comp = training_data_comp;
% % The number of Gaussian components used is hard coded
nogaussians_comp = input('Total number of gaussians to be used in fixed equilibrium point case');
[noVar_comp,noData_comp] = size(Data_operate_comp);
[Data_id_comp,Centres_comp] = kmeans(Data_operate_comp', nogaussians_comp);
Mu_k_comp = Centres_comp';
for i=1:nogaussians_comp
   idtmp_comp = find(Data_id_comp==i);
   Priors_k_comp(i) = length(idtmp_comp);
   Sigma_k_comp(:,:,i) = cov([Data_operate_comp(:,idtmp_comp) Data_operate_comp(:,idtmp_comp)]');
   %Add a tiny variance to avoid numerical instability
   Sigma_k_comp(:,:,i) = Sigma_k_comp(:,:,i) + 1E-5.*diag(ones(noVar_comp,1));
end
Priors_k_comp = Priors_k_comp ./ sum(Priors_k_comp);
[Priors1_comp, Mu1_comp, Sigma1_comp] = ExpMax(Data_operate_comp, Priors_k_comp, Mu_k_comp, Sigma_k_comp);
 
% % Creation of a mixed model
Priors_comp = Priors1_comp;
Mu_comp = Mu1_comp;
Sigma_comp = Sigma1_comp;
save('Priors_comp.mat','Priors_comp');
save('Mu_comp.mat','Mu_comp');
save('Sigma_comp.mat','Sigma_comp');

% calculating trainig error and validation error
[expData_training_comp(1:3,:),expSigma_training_comp] = GMR(Priors_comp, Mu_comp, Sigma_comp,  training_data_comp(1:3,:), 1:3, 4:6);
[expData_validating_comp(1:3,:),expSigma_validating_comp] = GMR(Priors_comp, Mu_comp, Sigma_comp,  validating_data_comp(1:3,:), 1:3, 4:6);
%% 
end
%%
% Amount of twist required by the velocity vector depends upon the distance of the present point
% from the equilibrium point.
% The concept of u is good but it distorts the original motion profile and generates something new
% 


end

% save('Priors_learnt','Priors');
% % fileIPrior = fopen('priors.txt','w');
% % % This will depend upon the number of gaussians manually choosen
% % priorstring = '%f';
% % for i = 1:nogaussians-2
% %    priorstring = strcat(priorstring,' %f'); 
% % end
% % priorstring = strcat(priorstring,' %f\n'); 
% % fprintf(fileIPrior,priorstring,Priors);
% dlmwrite('priors.txt', Priors, 'delimiter', '\t', 'precision', 32)
% %save('priors.txt','Priors','-ascii','-double');
% save('Mu_learnt','Mu');
% % fileIMean = fopen('mean.txt','w');
% % % This will depend upon the number of gaussians manually choosen
% % mustring = '%f';
% % for i = 1:nogaussians-2
% %    mustring = strcat(mustring,' %f'); 
% % end
% % mustring = strcat(mustring,' %f\n'); 
% % fprintf(fileIMean,mustring,Mu);
% dlmwrite('mean.txt', Mu, 'delimiter', '\t', 'precision', 32)
% %save('mean.txt','Mu','-ascii','-double');
% 
% save('Sigma_learnt','Sigma');
% % fileISigma = fopen('sigma.txt','w');
% % fprintf(fileISigma,'%f %f %f %f %f %f\n',Sigma);
% tmp_sigma = [];
% for i = 1:nogaussians
% tmp_sigma = [tmp_sigma;Sigma(:,:,i)];
% end
% %save('sigma.txt','tmp_sigma','-ascii','-double');
% dlmwrite('sigma.txt', tmp_sigma, 'delimiter', '\t', 'precision', 32)
% % X_M is the query point of the input
% % % Optional just for checking
% % [expData(1:noVar/2,:),expSigma] = GMR(Priors, Mu, Sigma,  eta, 1:3, 4:6);
% % Optional just for checking with validation data
% [expData(1:3,:),expSigma] = GMR(Priors, Mu, Sigma,  validating_data(1:5,:), 1:5, 6:8);
% % Writing sysconfig.txt file
% filesysconfig = fopen('sysconfig.txt','w');
% fprintf(filesysconfig,'num_input=5\n');
% fprintf(filesysconfig,'num_output=3\n');
% fprintf(filesysconfig,'num_lyp_asym_copont=0\n');
% fprintf(filesysconfig,'xi_star_training=[%f;%f;%f;%f;%f]', eta_star(1),eta_star(2),eta_star(3),eta_star(4),eta_star(5));
% 
% figure(1)
% plot(expData(1,:));
% hold on ;
% plot(validating_data(6,:),'r');
% figure(2)
% plot(expData(2,:));
% hold on ;
% plot(validating_data(7,:),'r');
% figure(3)
% plot(expData(3,:));
% hold on ;
% plot(validating_data(8,:),'r');





