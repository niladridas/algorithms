function T = frame_transformation(A_f,alpha_f,D_f,Theta_f)
A = [cos(Theta_f), -sin(Theta_f)*cos(alpha_f), sin(Theta_f)*sin(alpha_f), A_f*cos(Theta_f);
     sin(Theta_f), cos(Theta_f)*cos(alpha_f),-cos(Theta_f)*sin(alpha_f), A_f*sin(Theta_f);
     0,            sin(alpha_f),              cos(alpha_f),              D_f;
     0,            0,                         0,                         1];
T = A;
end
