clc
clear all
% The number of Gaussian components used is hard coded


nogaussians = input('Total number of gaussians to be used ');

% % % Calculating the Joint Probability Distribution Model

% % Using the k-means clustering algorithm to find the prior values of ...
% % the distribution model. The prior values are (component probabilities,
% % mean and covariance matrices)
% % Copied from http://programming-by-demonstration.org
% % Data_operate: NV x ND 
load('error_comp_matrix.mat');
Data_operate = error_comp_matrix;
% Data_operate(:,1:8) = 10*Data_operate(:,1:8); % The scaling has to be done ******* VERY IMPORTANT
Data_operate = Data_operate(1:80554,:)';
Data_operate(1:8,:) = 10*Data_operate(1:8,:);
[noVar,noData] = size(Data_operate);

[Data_id,Centres] = kmeans(Data_operate', nogaussians);

Mu_k = Centres'; % Mean position to initialize 

for i=1:nogaussians
  idtmp = find(Data_id==i);
  Priors_k(i) = length(idtmp);
  Sigma_k(:,:,i) = cov([Data_operate(:,idtmp) Data_operate(:,idtmp)]');
  %Add a tiny variance to avoid numerical instability
  Sigma_k(:,:,i) = Sigma_k(:,:,i) + 1E-5.*diag(ones(noVar,1));
end
Priors_k = Priors_k ./ sum(Priors_k);


% % Expectation Maximization Algorithm
% % Copied from http://programming-by-demonstration.org
% % Criterion to stop the EM iterative update

[Priors1, Mu1, Sigma1] = EM(Data_operate, Priors_k, Mu_k, Sigma_k);

% % Creation of a mixed model
Priors = Priors1;
Mu = Mu1;
Sigma = Sigma1;
%%
save('Priors_learnt','Priors');
save('Mu_learnt','Mu');
save('Sigma_learnt','Sigma');
% % This will depend upon the number of gaussians manually choosen
dlmwrite('priors.txt', Priors, 'delimiter', '\t', 'precision', 32)
dlmwrite('mean.txt', Mu, 'delimiter', '\t', 'precision', 32)
tmp_sigma = [];
for i = 1:nogaussians
tmp_sigma = [tmp_sigma;Sigma(:,:,i)];
end
dlmwrite('sigma.txt', tmp_sigma, 'delimiter', '\t', 'precision', 32)
% [expData(1:noVar/2,:),expSigma] = GMR(Priors, Mu, Sigma,  eta, 1:3, 4:6);
% Optional just for checking with validation data
[expData(1:4,:),expSigma] = GMR(Priors, Mu, Sigma,  Data_operate(1:8,:), 1:8, 9:12);
plot(Data_operate(10,:));
hold on ;plot(expData(2,:),'r')
