% The position velocity must be shifted a one step backward
% The previous position and velocity decides the next torque
% This has not been implemented


clear all
clc
% Load all data 
% Delete this section after once the parent_data is fromed
Pamans = [1, 1.05, 0;
          2, 1.05, 0;
          1, 1.40, 0;
          1, 1.22, 0;
          1.5, 1.22, 0;
          1, 0.7, 0;
          2, 0.7, 0;
          1.5, 0.7, 0;
          1, 0.525, 0;
          1.5, 0.525, 0;
          2, 0.525, 0;
          1, 0.35,0;
          1.5, 0.35,0;
          2, 0.35,0;
          0.2, 0.35, 0.1745;
          0.3, 0.35, 0.1745;
          0.1, 0.35, 0.1745;
          0.15, 0.35, 0.1745;
          0.4, 0.35, 0.1745;
          0.5, 0.35, 0.1745;
          0.6, 0.35, 0.1745;
          0.7, 0.35, 0.1745;
          0.8, 0.35, 0.1745;
          0.9, 0.35, 0.1745;
          1.0, 0.35, 0.1745;
          1.2, 0.35, 0.1745;
          1.4, 0.35, 0.1745;
          1.5, 0.35, 0.1745;
          1.6, 0.35, 0.1745;
          1.7, 0.35, 0.1745;
          1.8, 0.35, 0.1745;
          1.9, 0.35, 0.1745;
          2.0, 0.35, 0.1745];
%
% Load data 
% Parent data structure
Parent_Data = struct;
oldFolder = cd('/home/nilxwam/data_J2/');
% files = dir(['Filename_' yy(iy,:) mm(im,:) '*.dat']);
files = dir('*.txt');
for i=1:length(files)
    display(files(i).name);
    Parent_Data(i).index = i;
    Parent_Data(i).matrix = load(files(i).name,' -ascii');
    Parent_Data(i).size = size(Parent_Data(i).matrix);
    Parent_Data(i).sin_amplitude = [0 Pamans(i,2) 0 0];%input('Input the amplitude in radians as [j1 j2 j3 j4]: ');
    Parent_Data(i).sin_omega = [0 Pamans(i,1) 0 0];%input('Input the omega [wj1 wj2 wj3 wj4]: ');
    Parent_Data(i).start_pos = [0 Pamans(i,3) 0 +3.14];%input('Input the startpos in radian [startj1 startj2 startj3 startj4]: ');  
end
cd(oldFolder)
save('Parent_Data.mat', 'Parent_Data')


% We have a Parent_Data structure
% Since now we donot have a filter ready to find out noise free 
% acceleration and velocity from the system returned positions we will be
% using the data in the fields of the parent_data structure
% TODO : create a proper robust online and offline filter or anything that 
% gives a noise free derivative

%This specific module will be changed if the data acquition format is
%changed
% Whatever changed may be done the first column of each matrix is TIME

% This part needs to be written in future 
%
DOF = 4;
load('Parent_Data.mat');
FILTER_STATE = input('Enter 1 if filter is working, else 0: ');
if(FILTER_STATE == 0)
   % Each matrix has to be modified to fill it with proper velocity and 
   % acceleration terms
   % matrix is stored in Parent_Data(i).matrix
   for i=1:Parent_Data(end).index
       for j = 1:DOF % modifying velocity and acceleration for each joint
       Parent_Data(i).matrix(:,5+j) = Parent_Data(i).sin_omega(j)*Parent_Data(i).sin_amplitude(j)*sin(Parent_Data(i).sin_omega(j).*Parent_Data(i).matrix(:,1));% Velocity (sine signal)
       Parent_Data(i).matrix(:,9+j) = Parent_Data(i).sin_omega(j)*Parent_Data(i).sin_omega(j)*Parent_Data(i).sin_amplitude(j)*sin(Parent_Data(i).sin_omega(j).*Parent_Data(i).matrix(:,1));% Acceleration (sine signal)
       end
   end
   save('Parent_Data.mat', 'Parent_Data')
end
if(FILTER_STATE == 1)
end
if(FILTER_STATE ~= 1 && (FILTER_STATE ~= 0))
    display('Error: Filter state should be either 0 or 1')
end