% Two types of update is going on
% Weight Updates --> Updated V representation --> Updated U representation

% This function takes the value function and returns the V_W and V_X
function [V_x,V_w,Value_function] = value_function_derivative(Input, Weights, value_function_tmp)

% First find the symbolic derivatives
    Value_function = value_function_tmp(Input, Weights);
    size_input = size(Input,1);
    syms ip1 ip2 real
    Input_sym = [ip1;ip2];
%     Input_sym = sym('IP',[1 size_input])';
%     for i= 1:size_input
%     sym(Input_sym(i),'real')
%     end
%     assume(Input_sym,'integer')
%     Input_sym = sym(Input_sym,'real');
%     Input_sym =   real(Input_sym);
    %display(Input_sym);
    syms w1 w2 real
    Weight_sym = [w1;w2];
%     Weight_sym = sym('Wt',[1 size_input])';
%     for i= 1:size_input
%     sym(Weight_sym(i),'real')
%     end
%     Weight_sym = sym(Weight_sym,'real');
    %display(Weight_sym);
%     display(size_input);
    V_x = sym(zeros(size_input,1));
    V_w = sym(zeros(size_input,1));
    Value_function_sym = value_function_tmp(Input_sym, Weight_sym);
    %display(Value_function_sym)
    for i = 1: size_input
        V_x(i) = diff(Value_function_sym,Input_sym(i));
        V_x(i) = eval(subs(V_x(i),[Input_sym,Weight_sym],[Input, Weights]));
        V_w(i) = diff(Value_function_sym, Weight_sym(i));
        V_w(i) = eval(subs(V_w(i),[Input_sym,Weight_sym],[Input, Weights]));
    end
    
 %-------------------------------------   
end