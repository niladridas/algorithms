% Simulation System
% This system will simulate the real system of 4 Dof Barrett WAM:
% Have as an input the 4 torques:
% The output of the system are the states position, velocity
function X = system_4dof(torque, init_pos, init_vel, Ts)
    M_inv = M_full(init_pos)\1;
    X = [init_pos;init_vel] + Ts*[init_vel; M_inv*torque - M_inv*C_full([init_pos;init_vel])];
end