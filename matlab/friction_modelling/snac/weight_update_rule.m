% Weight update rule
function W_dot = weight_update_rule(Input,Q,Weights,value_function_derivative_tmp,value_function_tmp,dynamics_tmp)
% We need to know the present state values
% Q and R of the cost function
% Value fucntion derivatives
% F and G from the dynamics
[V_x,V_w,Value_function] = value_function_derivative_tmp(Input, Weights, value_function_tmp);
[X_dot,F,G] = dynamics_tmp(Input);
r = -0.5*Input'*Q*Input + 0.5*V_x'*(G*G')*V_x -Input'*F;
%display(r)
s = V_w';
%display(s)
W_dot = s'*(1/(s*s'))*r;
end