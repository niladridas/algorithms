clc
clear all
delta_t = 0.001;
eta1 = 0.2;
eta2 = 0.3;
ND = 10;
X0 = [-1;1];
W0 = [0.6;0.5];
U0 = 0;
U = zeros(1,ND);
W = zeros(2,ND);
X = zeros(2,ND);
Vx = zeros(2,ND);
Vw = zeros(2,ND);
Xdot = zeros(2,ND);
for i = 1:ND
% Loop 1
[X_dot,F,G] = dynamics(X0,U0);
[V_x,V_w,Value_function] = value_function_derivative(X0, W0, @value_function);
U(i) = -G'*V_x;
Vx(:,i) = V_x;
U0 = U(i);
X0 = X0 + delta_t*X_dot;
X(:,i) = X0;
Xdot(:,i) = X_dot;

% Loop 2
W_dot = weight_update_rule(X0,[1,0;0,1],W0,@value_function_derivative,@value_function,@dynamics);
W0 = W0 + [eta1;eta2].*W_dot;
W(:,i) = W0;
end