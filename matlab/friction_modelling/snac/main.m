% Policy iteration technique

% First initialize with a value for u(k) and lamda(k), say equal to zero
% Inside a loop first:
%              lamda(k+1) is estimated based on the initial u and lamda
%              This lamda(k+1) gives us the new u
% Continue

% constant values:
init_position = [0;0;0;pi];
init_velocity = [0;0;0;0];
init_acc = [0;0;0;0];

des_position = 