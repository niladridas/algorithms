% Pendulum dynamics
function [X_dot,F,G] = dynamics(varargin)%dynamics(X,U)
    if nargin > 2
        error('myApp:argChk', 'Wrong number of input arguments, only 2 inputs possible X then U')
    end
    if nargin == 2
        %display('Both X and U have been provided')
    end
    if nargin == 1
        %display('Only X has been provided')
    end
%     X_dot = sym(zeros(size(varargin{1},1)));
    X_dot = zeros(size(varargin{1},1),1);

    for i = 1:size(varargin{1},1)
        F = [varargin{1}(2);-10*sin(varargin{1}(1))];
        G = [0;1];
        if nargin == 2
            X_dot = F + G*varargin{2};
        else
            X_dot = NaN;
        end
        
    end
end