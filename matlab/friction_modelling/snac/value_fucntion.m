% This fucntion calculates the V the value function
% The input is the input vector and the weight vector
% This fucntion can be used as single time or over multiple time instants
function V = value_function(Input, Weights)
% Input No_states X no_data
% Weights No_weight_state X no_data
if size(Input,2)~=size(Weights,2)
    error('myApp:argChk', 'Wrong number of input arguments')
end
V = sym(zeros(1,size(Input,2)));
for i = 1:size(Input,2)
    V(i) = 0.5*( (Weights(:,i)'*Input(:,i))*(Weights(:,i)'*Input(:,i)) + (Weights'*Weights)); 
end