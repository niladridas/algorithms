clear all;
clc;
Raw = load('/home/nilxwam/Dropbox/aritra/4dof/29_01_2015_4dof_14.txt');
Aritra = Raw(:,2:21);
 
 theta = Aritra(:,1:4);
 theta_dot = Aritra(:,5:8);
% theta_dot_dot= Aritra(:,9:12);
 theta_dot_dot = zeros(size(Aritra,1),4);
 theta_dot_dot(:,4) = -0.525*0.5*0.5*sin(0.5*Raw(:,1)); 
 torque = Aritra(:,13:16);
 G = Aritra(:,17:20);

 
%  model_torque_reduced = zeros(size(torque));
 model_torque = zeros(size(torque));
 final_torque = zeros(size(torque));
 model_gravity =  zeros(size(torque));
 A_saved = zeros(1,size(torque,1));
 B_saved = zeros(1,size(torque,1));
 M22 = zeros(1,size(torque,1));
 
 for i = 1:size(torque,1)
     A = M_full([theta(i,:)';theta_dot(i,:)'])*theta_dot_dot(i,:)';
     A_saved(1,i) = A(2,1);
     M = M_full([theta(i,:)';theta_dot(i,:)']);
     M22(1,i) = M(2,2);
     B = C_full([theta(i,:)';theta_dot(i,:)']);
     B_saved(1,i) = B(2,1);
     C = A+B;
     model_torque(i,:) = C';
     tmp_G = gravity(theta(i,:))';
     model_gravity(i,:) = tmp_G;
     final_torque(i,:) = C'+tmp_G;
 end
 
 
 figure(1)
 %plot(theta(1000:2000,2));
 plot(final_torque(:,4));
 hold on
 plot(torque(:,4)+G(:,4),'r');
% 
% %  figure(2)
%   plot(model_torque(:,2)./torque(:,2),'r');
% %  hold on
% %  plot(torque(3266:6400,2),'g');
% %  
% 
% %plot((torque(10:816,2)-model_torque(10:816,2))./torque(10:816,2),'r');
% figure(2)
% plot(Vel(:,2));
% hold on;
% plot(theta_dot(:,2));
