clc
clear all

load('Parent_Data'); % This will load a structure called Parent_Data
error_comp_matrix = []; % position, velocity, error to compensate
for i=1:Parent_Data(end).index % Taking one matrix at a time
    % Calculate the model torque ie M and C only
    tmp_matrix = zeros(size(Parent_Data(i).matrix,1),12);
    tmp_matrix(:,1:4) = Parent_Data(i).matrix(:,2:5);
    tmp_matrix(:,5:8) = Parent_Data(i).matrix(:,6:9);
    theta_dot_dot = Parent_Data(i).matrix(:,10:13);
    % M and C takes row or column vector, ammend this so that M and C can
    % take matrices also
    for j = 1:size(tmp_matrix,1)
     Tau = M_full(tmp_matrix(j,1:4))*theta_dot_dot(j,:)' + C_full([tmp_matrix(j,1:4)';tmp_matrix(j,5:8)']);
     tmp_matrix(j,9:12) = Tau';
    end
    tmp_matrix(:,9:12) = Parent_Data(i).matrix(:,14:17) -  tmp_matrix(:,9:12);
    error_comp_matrix = [error_comp_matrix;tmp_matrix];
end
save('error_comp_matrix.mat','error_comp_matrix')
%Training_Matrix = zeros()