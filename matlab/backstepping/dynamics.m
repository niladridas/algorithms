function Dx = dynamics(t,x,Mf,Cf,refsignal,A,w) % Here x = [position;velocity]
L1 = 10;
L2 = 10;
global U;
global count;
count = count +1;
M = Mf(x(1:4,1));
C = Cf(x(1:8,1));
[xd,xd_d,xd_dd] = refsignal(t,A,w);
Dx = zeros(16,1);
Dx(1:4,1) = x(5:8,1);
% Dx(5:8) = -(M\C) + [0.4;2*sin(w*t);0.6;0.9] + ((M + [0,0,0,0.4;0.2*sin(w*t),0.6,0.3,0.5;0.1,0,0.11,0.3;0,0.12,0,0.19])\(M*( -(M\C) -K1*K2*(x(1:4,1)-xd) + (K1+K2)*(xd_d - x(5:8,1)) + xd_dd))); 
T1 = [x(9),0,0,0;0,x(10),0,0;0,0,x(11),0;0,0,0,x(12)];
T2 = [x(13),0,0,0;0,x(14),0,0;0,0,x(15),0;0,0,0,x(16)];
%T1 = [1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
%T2 = [4,0,0,0;0,4,0,0;0,0,4,0;0,0,0,4];
Dx(5:8) = -(M\C) + ((M )\(M*( (M\C) -T1*T2*(x(1:4,1)-xd) + (T1+T2)*(xd_d - x(5:8,1)) + xd_dd)));
%Dx(5:8) = -(M\C) +  ((M )\(M*( -(M\C) -T1*T2*(x(1:4,1)-xd) + (T1+T2)*(xd_d - x(5:8,1)) + xd_dd))); 
U = [U,((M*( (M\C) -T1*T2*(x(1:4,1)-xd) + (T1+T2)*(xd_d - x(5:8,1)) + xd_dd)))];
e_x = ((x(1:4)-xd)./A).*((x(1:4)-xd)./A);
e_xd = ((x(5:8)-xd_d)./(A*w)).*((x(5:8)-xd_d)./(A*w));

if e_x(1) > 0.010
    Dx(9) = L1*(e_x(1));
elseif e_x(1) < 0.0064
    Dx(9) = -L2*(e_x(1));
else 
    Dx(9) = 0;
end

if e_x(2) > 0.010
    Dx(10) = L1*(e_x(2));
elseif e_x(2) < 0.0064
    Dx(10) = -L2*(e_x(2));    
else 
    Dx(10) = 0;
end

if e_x(3) > 0.010
    Dx(11) = L1*(e_x(3));
elseif e_x(3) < 0.0064
    Dx(11) = -L2*(e_x(3));    
else 
    Dx(11) = 0;
end

if e_x(4) > 0.010
    Dx(12) = L1*(e_x(4));
elseif e_x(4) < 0.0064
    Dx(12) = -L2*(e_x(4));    
else 
    Dx(12) = 0;
end

if e_xd(1) > 0.010
    Dx(13) = L1*(e_xd(1));
elseif e_xd(1) < 0.0064
    Dx(13) = -L2*(e_xd(1));    
else 
    Dx(13) = 0;
end

if e_xd(2) > 0.010
    Dx(14) = L1*(e_xd(2));
elseif e_xd(2) < 0.0064
    Dx(14) = -L2*(e_xd(2));     
else 
    Dx(14) = 0;
end

if e_xd(3) > 0.010
    Dx(15) = L1*(e_xd(3));
elseif e_xd(3) < 0.0064
    Dx(15) = -L2*(e_xd(3));     
else 
    Dx(15) = 0;
end

if e_xd(4) > 0.010
    Dx(16) = L1*(e_xd(4));
elseif e_xd(4) < 0.0064
    Dx(16) = -L2*(e_xd(4));     
else 
    Dx(16) = 0;
end

