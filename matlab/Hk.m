function datahk = Hk(Data, Priors,Mu, Sigma,gaussPdf_nilu) % Here Mu is No_state x no_gauss
    % Here Sigma is (No_input + No_output)^k
    % Priors is a column vector
    outPdf
    for i = 1:size(Priors,1)
    outPdf = gaussPdf_nilu(Data, Mu, Sigma);
    datahk = Priors
end