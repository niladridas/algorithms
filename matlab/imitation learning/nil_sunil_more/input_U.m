% Calculating input U at a particular query point
function input_zeta = input_U(gradient_of_V,eta_dot_estimate,eta, eta_star)
global rho_0;
global K_0;
nbrow_eta = size(eta,1);
if(((gradient_of_V'*eta_dot_estimate) + (rho_0*(1 - exp(-K_0*norm(eta)))) <= 0) && (norm(eta-eta_star) >= 0.005))
    input_zeta = zeros(nbrow_eta,1);
elseif((norm(eta-eta_star) <= 0.005))
    input_zeta = -eta_dot_estimate;
% elseif(((gradient_of_V'*eta_dot_estimate) + (rho_0*(1 - exp(-K_0*norm(eta)))) >= 0) && (norm(eta-eta_star) > 0.01)) 
else
    input_zeta = -gradient_of_V*((gradient_of_V'*eta_dot_estimate + rho_0*(1 - exp(-K_0*norm(eta))))/...
        (gradient_of_V'*gradient_of_V)); 
end