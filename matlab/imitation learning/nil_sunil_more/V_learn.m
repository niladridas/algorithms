% Learning V depending upon only three variables x,y,z
clc
clear all
all_data= load('abc'); % All data is here
[m,n]=size(all_data); % Determine size of data
p=zeros(2,1); % save index of consecutive break point denoting change in demonstrations
p(1,1) = 0;
count = 0;
temp_cell={};% define cell to store diff demonstrations
for i=1:m-1 
   if all_data(i+1,1) < all_data(i,1)
       count = count +1;
        p(2,1)=i;
        temp_cell{count,1} = all_data(p(1,1)+1:p(2,1),2:7); % considering the first two joint angles
        p(1,1)=i;
   end
    if i == m-1
          temp_cell{count+1,1} = all_data(p(1,1)+1:m,2:7); 
          end
    i=i+1;
end

[total_no_demo, col_temp_cell] = size(temp_cell);
fprintf('Number of demonstrations %i\n', total_no_demo);
valid_demo_no = input('enter which demonstration to be taken for validation  ');

training_data = [];

for i=1:total_no_demo
	if i~= valid_demo_no 
        training_data = cat(2,training_data,temp_cell{i}');
    elseif i== valid_demo_no
		validating_data =  temp_cell{valid_demo_no}';
    end
end

temp_ip_op = training_data';
% save('input_output.txt','temp_ip_op','-ascii','-double');
% save('validating_data_mat','validating_data');
% tmp_valid = validating_data';
% save('validation.txt','tmp_valid','-ascii','-double');
%%----------------------------------------------------------------------------------------------------------%%

% First calculate the equilibrium point
% This value of x,y,z co-ordinate should come as an external input
% But for now we are just assuming it to be the average value of the end point from all the demonstrations

end_position_vector = [0;0;0];

for i = 1:total_no_demo
	temp_demo_matrix = temp_cell{i};
	end_position_vector = (temp_demo_matrix(end,1:3)')/total_no_demo + end_position_vector;
end
%% --------------------------------------------------------------------------------------------------------
%%-----------------------------------------------------------------------------------------------------------%%

% Finding out V 
global eta_star ;
global eta_col;
global w_bar;
global eta_row;
global L;
global eta ;
global eta_dot ;

eta_star = end_position_vector;
save('eta_star_mat','eta_star');
eta = training_data(1:3,:);
eta_dot = training_data(4:6,:);

[eta_row,eta_col] = size(eta);

w_bar = 0.1;%%---------------------------------------------------------

fprintf('The value of the weight factor set is %d\n', w_bar);

% We set the number of asymmetric components to be L
L =0; %%---------------------------------------------------------------
if (L==0)
      x0 = eye(eta_row,eta_row);
end

x = fmincon(@cost_function,x0,[],[],[],[],[],[],@positive_def);

save('P_values','x'); % This variable contains P0, Pl and mul
P0 = x(1:3,1:3);

%save('P0.txt','P0','-ascii','-double');
dlmwrite('P0.txt', P0, 'delimiter', '\t', 'precision', 32);