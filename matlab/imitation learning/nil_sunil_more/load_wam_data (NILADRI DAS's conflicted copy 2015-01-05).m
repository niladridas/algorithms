%% ----------------------------------------------------------------------
clc
clear all
%all_data= load('22122014_3.txt'); % All data is here
all_data= load('abc'); % All data is here
[m,n]=size(all_data); % Determine size of data

p=zeros(2,1); % save index of consecutive break point denoting change in demonstrations

p(1,1) = 0;

count = 0;

temp_cell={};% define cell to store diff demonstrations

for i=1:m-1 
   if all_data(i+1,1) < all_data(i,1)
       count = count +1;
        p(2,1)=i;
        temp_cell{count,1} = all_data(p(1,1)+1:p(2,1),2:7);
        p(1,1)=i;
   end
    if i == m-1
          temp_cell{count+1,1} = all_data(p(1,1)+1:m,2:7); 
          end
    i=i+1;
end

[total_no_demo, col_temp_cell] = size(temp_cell);
fprintf('Number of demonstrations %i\n', total_no_demo);
valid_demo_no = input('enter which demonstration to be taken for validation  ');

training_data = [];

for i=1:total_no_demo
	if i~= valid_demo_no 
        training_data = cat(2,training_data,temp_cell{i}');
    elseif i== valid_demo_no
		validating_data =  temp_cell{valid_demo_no}';
    end
end

%fileIPOP = fopen('input_output.txt','w');
%fprintf(fileIPOP,'%f %f %f %f %f %f\n',training_data');
temp_ip_op = training_data';
save('input_output.txt','temp_ip_op','-ascii','-double');
save('validating_data_mat','validating_data');
tmp_valid = validating_data';
save('validation.txt','tmp_valid','-ascii','-double');
%fileIValid = fopen('validation.txt','w');
%fprintf(fileIValid,'%f %f %f %f %f %f\n',validating_data');


%% ----------------------------------------------------------------------------------------------------------%%

% First calculate the equilibrium point
% This value of x,y,z co-ordinate should come as an external input
% But for now we are just assuming it to be the average value of the end point from all the demonstrations

end_position_vector = [0;0;0];

for i = 1:total_no_demo
	temp_demo_matrix = temp_cell{i};
	end_position_vector = (temp_demo_matrix(end,1:3)')/total_no_demo + end_position_vector;
end
%% --------------------------------------------------------------------------------------------------------
%%-----------------------------------------------------------------------------------------------------------%%

% Finding out V 
global eta_star ;
global eta_col;
global w_bar;
global eta_row;
global L;
global eta ;
global eta_dot ;

eta_star = end_position_vector;
save('eta_star_mat','eta_star');
eta = training_data(1:3,:);
eta_dot = training_data(4:6,:);

[eta_row,eta_col] = size(eta);

w_bar = 0.1;%%---------------------------------------------------------

fprintf('The value of the weight factor set is %d\n', w_bar);

% We set the number of asymmetric components to be L
L =0; %%---------------------------------------------------------------

fprintf('The number of assymetric components is set to be %d\n', L);
if (L~=0)
    x0 = rand(eta_row+eta_row*L+L,eta_row);
    x0(1:(L+1)*eta_row,1:eta_row) = repmat(eye(eta_row),L+1,1);
    x0((L+1)*eta_row+1:(L+1)*eta_row+L,1:eta_row) = 10*rand(L,eta_row);
end

if (L==0)
      x0 = eye(eta_row,eta_row);
end

x = fmincon(@cost_function,x0,[],[],[],[],[],[],@positive_def);

save('P_values','x'); % This variable contains P0, Pl and mul
P0 = x(1:3,1:3);

%save('P0.txt','P0','-ascii','-double');
dlmwrite('P0.txt', P0, 'delimiter', '\t', 'precision', 32)
%fileP0 = fopen('P0.txt','w');
%fprintf(fileP0,'%f %f %f\n',P0);

if(L~=0)
P_total1 = x(4:4+L*3,1:3);
%save('P_total1.txt','P_total1','-ascii','-double');
%fileP_total1 = fopen('P_total1.txt','w');
%fprintf(fileP0,'%f %f %f\n',P_total1);
Mu_total = x((L+1)*3+1:end,1:3);
%save('Mu_total.txt','Mu_total','-ascii','-double');
%fileMu_total = fopen('Mu_total.txt','w');
%fprintf(fileMu_total,'%f %f %f\n',Mu_total);
end


%%-------------------------------------------------------------------------------------------------------------%%

% Filtering out the observed data according to the Lyapunov criteria
a_count = 1;
Filtered_data = [];
for i = 1:size(training_data,2)
    if(  (gradient_V(x ,training_data(1:3,i),eta_star)'*(training_data(4:6,i))) < 0)
        Filtered_data(a_count,:) = training_data(:,i)';
        a_count = a_count+1;
    end
end

%%----------------------------------------------------------------------------------------------------------%%
[row_Filtered_data, col_Filtered_data] = size(Filtered_data);

% Restructuring the data as first the state data as (no. of states X ...
% Total time stamp) and immediately below we have the state derivatives ...
% in the same fashion as the states

Data_operate = Filtered_data';

% The number of Gaussian components used is hard coded
nogaussians = input('Total number of gaussians to be used ');

% % % Calculating the Joint Probability Distribution Model

% % Using the k-means clustering algorithm to find the prior values of ...
% % the distribution model. The prior values are (component probabilities,
% % mean and covariance matrices)
% % Copied from http://programming-by-demonstration.org

[noVar,noData] = size(Data_operate);
[Data_id,Centres] = kmeans(Data_operate', nogaussians);
Mu_k = Centres';
for i=1:nogaussians
  idtmp = find(Data_id==i);
  Priors_k(i) = length(idtmp);
  Sigma_k(:,:,i) = cov([Data_operate(:,idtmp) Data_operate(:,idtmp)]');
  %Add a tiny variance to avoid numerical instability
  Sigma_k(:,:,i) = Sigma_k(:,:,i) + 1E-5.*diag(ones(noVar,1));
end
Priors_k = Priors_k ./ sum(Priors_k);


% % Expectation Maximization Algorithm
% % Copied from http://programming-by-demonstration.org
% % Criterion to stop the EM iterative update

[Priors1, Mu1, Sigma1] = EM(Data_operate, Priors_k, Mu_k, Sigma_k);

% % Creation of a mixed model
Priors = Priors1;
Mu = Mu1;
Sigma = Sigma1;
%%
save('Priors_learnt','Priors');
% fileIPrior = fopen('priors.txt','w');
% % This will depend upon the number of gaussians manually choosen
% priorstring = '%f';
% for i = 1:nogaussians-2
%    priorstring = strcat(priorstring,' %f'); 
% end
% priorstring = strcat(priorstring,' %f\n'); 
% fprintf(fileIPrior,priorstring,Priors);
dlmwrite('priors.txt', Priors, 'delimiter', '\t', 'precision', 32)
%save('priors.txt','Priors','-ascii','-double');
save('Mu_learnt','Mu');
% fileIMean = fopen('mean.txt','w');
% % This will depend upon the number of gaussians manually choosen
% mustring = '%f';
% for i = 1:nogaussians-2
%    mustring = strcat(mustring,' %f'); 
% end
% mustring = strcat(mustring,' %f\n'); 
% fprintf(fileIMean,mustring,Mu);
dlmwrite('mean.txt', Mu, 'delimiter', '\t', 'precision', 32)
%save('mean.txt','Mu','-ascii','-double');

save('Sigma_learnt','Sigma');
% fileISigma = fopen('sigma.txt','w');
% fprintf(fileISigma,'%f %f %f %f %f %f\n',Sigma);
tmp_sigma = [];
for i = 1:nogaussians
tmp_sigma = [tmp_sigma;Sigma(:,:,i)];
end
%save('sigma.txt','tmp_sigma','-ascii','-double');
dlmwrite('sigma.txt', tmp_sigma, 'delimiter', '\t', 'precision', 32)
% X_M is the query point of the input
% % Optional just for checking
% [expData(1:noVar/2,:),expSigma] = GMR(Priors, Mu, Sigma,  eta, 1:3, 4:6);
% Optional just for checking with validation data
[expData(1:noVar/2,:),expSigma] = GMR(Priors, Mu, Sigma,  validating_data(1:3,:), 1:3, 4:6);
% Writing sysconfig.txt file
filesysconfig = fopen('sysconfig.txt','w');
fprintf(filesysconfig,'num_input=3\n');
fprintf(filesysconfig,'num_output=3\n');
fprintf(filesysconfig,'num_lyp_asym_copont=0\n');
fprintf(filesysconfig,'xi_star_training=[%f;%f;%f]', eta_star(1),eta_star(2),eta_star(3));

figure(1)
plot(expData(1,:));
hold on ;
plot(validating_data(4,:),'r');
figure(2)
plot(expData(2,:));
hold on ;
plot(validating_data(5,:),'r');
figure(3)
plot(expData(3,:));
hold on ;
plot(validating_data(6,:),'r');


