% STEP 1

nbfiles = input('enter number of observation files\n');
first_str = '/home/niladriisl/Dropbox/RA_work/Imitation Learning/new_codes/Data_file/a';
nbvariables = input('enter number of states\n');

raw_data = [];
last_row_tmp =zeros(1,nbvariables);
for i = 1:nbfiles
    
    file_id = strcat(first_str,int2str(i),'.txt');
%     raw_data_temp(:,:,i) = csvread(file_id); % CHANGE HERE
    temp_raw_data_temp = csvread(file_id);
    [row_temp_raw_data_temp,col_temp_raw_data_temp] = size(temp_raw_data_temp);
    raw_data = cat(1,raw_data,temp_raw_data_temp);
    display('HELP')
    % Saving the last row of each single demonstration
    last_row_tmp = last_row_tmp + temp_raw_data_temp(row_temp_raw_data_temp,1:col_temp_raw_data_temp/2)./nbfiles; 
end



raw_data = raw_data';
[row_raw_data, column_raw_data] = size(raw_data);
global eta ;
eta = raw_data(1:row_raw_data/2,:);
global eta_dot ;
eta_dot = raw_data((row_raw_data/2)+1:row_raw_data,:);

global eta_star;
% Averaging over the end position of the motion primitive %%------------------------------------------------------------------ 
% Very very crude technique
% [row_raw_data_temp, col_raw_data_temp, hght_raw_data_temp] =...
%     size(raw_data_temp);
% temp_eta_star = zeros(hght_raw_data_temp,col_raw_data_temp/2);
% for i = 1:hght_raw_data_temp
%     for j = 1:col_raw_data_temp/2
%     temp_eta_star(i,j) = raw_data_temp(row_raw_data_temp,...
%         j,i);
%     end
% end
% eta_star = sum(temp_eta_star)/(hght_raw_data_temp); % CHANGE HERE
% eta_star = last_row_tmp;
eta_star = [0,0];
eta_star = eta_star';
save('eta_star_mat','eta_star');

global eta_col;
global w_bar;
global eta_row;
global L;

[eta_row,eta_col] = size(eta);
w_bar = 0.1;%%----------------------------------------------------------------------------------------------------------------
% end
% load_counter = 1;
% We set the number of asymmetric components to be L
L =3; %%---------------------------------------------------------------------------------------------------------------------
if (L~=0)
    x0 = rand(eta_row+eta_row*L+L,eta_row);
    x0(1:(L+1)*eta_row,1:eta_row) = repmat(eye(eta_row),L+1,1);
    x0((L+1)*eta_row+1:(L+1)*eta_row+L,1:eta_row) = 10*rand(L,eta_row);
end
if (L==0)
%     x0 = 0.01*rand(eta_row,eta_row);
      x0 = eye(eta_row,eta_row);
end
x = fmincon(@cost_function,x0,[],[],[],[],[],[],@positive_def);
save('P_values','x');
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% TRAINING OF LYAPUNOV FUNCTION FINISHED
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %


%%
% Filtering out the observed data according to the Lyapunov criteria
% This step will be dicarded if the number of observation points falls
% below certain percentage of the population.
% Hence we need to calculate V_dot = (grad_V)'*eta_dot
% a_count = 1;
% for i = 1:hght_raw_data_temp
%     for j = 1:row_raw_data_temp
%         % First calculate grad_V
%         if((gradient_V(x,eta_t,eta_star))'*(raw_data_temp(j,1:col_raw_data_temp/2,hght_raw_data_temp)') < 0)
%             M(a,:) = raw_data_temp(j,1:col_raw_data_temp/2,hght_raw_data_temp);
%             a_count = a_count+1;
%         end
%     end
% end

a_count = 1;
for i = 1:size(raw_data,2)
    if((gradient_V(x,raw_data(1:size(raw_data,1)/2,i),eta_star))'*(raw_data(1+size(raw_data,1)/2:size(raw_data,1),i)) < 0)
        M(a_count,:) = raw_data(:,i)';
        a_count = a_count+1;
    end
end

%%








% Demonstration/filtered data are assumed to be in workspace or in some
% text file

% Data available in a text file in form of comma separated values
% The data in the text file is available in column wise order
%M = csvread('filtered_data.txt'); % some csv data file
% Once we have the numeric data in the matrix M , simply 0.5*(no,. of ...
% columns of M) gives us the order of the system.

[row_M, col_M] = size(M);

% Restructuring the data as first the state data as (no. of states X ...
% Total time stamp) and immediately below we have the state derivatives ...
% in the same fashion as the states
Data_operate = M';

% The number of Gaussian components used is hard coded
nogaussians = 6 ; % Changed according to the user    %%-------------------------------------------------------------------

% % % Calculating the Joint Probability Distribution Model

% % Using the k-means clustering algorithm to find the prior values of ...
% % the distribution model. The prior values are (component probabilities,
% % mean and covariance matrices)
% % Copied from http://programming-by-demonstration.org

[noVar,noData] = size(Data_operate);
[Data_id,Centres] = kmeans(Data_operate', nogaussians);
Mu_k = Centres';
for i=1:nogaussians
  idtmp = find(Data_id==i);
  Priors_k(i) = length(idtmp);
  Sigma_k(:,:,i) = cov([Data_operate(:,idtmp) Data_operate(:,idtmp)]');
  %Add a tiny variance to avoid numerical instability
  Sigma_k(:,:,i) = Sigma_k(:,:,i) + 1E-5.*diag(ones(noVar,1));
end
Priors_k = Priors_k ./ sum(Priors_k);


% % Expectation Maximization Algorithm
% % Copied from http://programming-by-demonstration.org
% % Criterion to stop the EM iterative update

[Priors1, Mu1, Sigma1] = EM(Data_operate, Priors_k, Mu_k, Sigma_k);

% % Creation of a mixed model
Priors = Priors1;
Mu = Mu1;
Sigma = Sigma1;

save('Priors_learnt','Priors');
save('Mu_learnt','Mu');
save('Sigma_learnt','Sigma');


% X_M is the query point of the input
% Optional just for checking
[expData(1:noVar/2,:),expSigma] = GMR(Priors, Mu, Sigma,  eta, 1:2, 3:noVar);





