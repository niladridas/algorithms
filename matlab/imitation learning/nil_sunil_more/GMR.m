function [y, Sigma_y] = GMR(Priors11, Mu11, Sigma11, x11, in1, out1)


% nbData1 = length(x11);
nbData1 = size(x11,2);
% display(nbData1);
nbVar1 = size(Mu11,1);
nbStates1 = size(Sigma11,3);

% % Compute the influence of each GMM component, given input x

for i=1:nbStates1
  Pxi(:,i) = Priors11(i).*gaussPDF(x11, Mu11(in1,i), Sigma11(in1,in1,i));
end
beta = Pxi./repmat(sum(Pxi,2)+realmin,1,nbStates1);
% % Compute expected means y, given input x

for j=1:nbStates1
  y_tmp(:,:,j) = repmat(Mu11(out1,j),1,nbData1) + Sigma11(out1,in1,j)*inv(Sigma11(in1,in1,j)) * (x11-repmat(Mu11(in1,j),1,nbData1));
end
beta_tmp = reshape(beta,[1 size(beta)]);
y_tmp2 = repmat(beta_tmp,[length(out1) 1 1]) .* y_tmp;
%display(y_tmp2);
y = sum(y_tmp2,3);
% % Compute expected covariance matrices Sigma_y, given input x

for j=1:nbStates1
  Sigma_y_tmp(:,:,1,j) = Sigma11(out1,out1,j) - (Sigma11(out1,in1,j)*inv(Sigma11(in1,in1,j))*Sigma11(in1,out1,j));
end
beta_tmp = reshape(beta,[1 1 size(beta)]);
Sigma_y_tmp2 = repmat(beta_tmp.*beta_tmp, [length(out1) length(out1) 1 1]) .* repmat(Sigma_y_tmp,[1 1 nbData1 1]);
Sigma_y = sum(Sigma_y_tmp2,4);

