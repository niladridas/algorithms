signal = diff2_5(500:end,23);
N = length(signal);
fs = 500; % 62.5 samples per second
fnyquist = fs/2; %Nyquist frequency
plot(abs(fft(signal)))
xlabel('Frequency (Bins - almost!)')
ylabel('Magnitude');
title('Double-sided Magnitude spectrum');
axis tight