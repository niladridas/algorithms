%
display('The filter length must be odd ')
N = input('Enter the length of the filter ');
M = (N-1)/2;
% Calculating coeff S
S = zeros(M+1,1);
S(M,1) = 1;
for i=M-1:-1:1
    S(i) = ((2*N-10)*S(i+1) - (N+2*i+3)*S(i+2))/(N-2*i-1) 
end
S0 = ((2*N-10)*S(1) - (N+3)*S(2))/(N-1)