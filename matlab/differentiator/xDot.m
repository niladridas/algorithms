function out = xDot(data,dataIndx_In,ts)


    x = data(:,dataIndx_In);
    [d,s]=size(x);
    clear ddot;
    for j=1:d
        if j==1
        x_dot(j,:)=zeros(1,s)    ;
        else
        x_dot(j,:)=(x(j,:)-x(j-1,:))/ts;
        end
    end
   
    

out=x_dot;
end
