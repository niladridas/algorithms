#include <Python.h>
#include <dlfcn.h>

int
main(int argc, char *argv[])
{
dlopen("libpython2.7.so.1.0", RTLD_NOW | RTLD_NOLOAD | RTLD_GLOBAL);
  Py_SetProgramName(argv[0]);  /* optional but recommended */
  Py_Initialize();
  PyRun_SimpleString("from time import time,ctime\n"
                     "print 'Today is',ctime(time())\n");
  Py_Finalize();
  return 0;
}
