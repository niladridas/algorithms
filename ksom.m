% 3 DIMENSIONAL SOM (a,b,c) (12,7,4)
% Input vector size = IP_N
clear all

a = 12;
b = 7;
c = 4;
IP_N = 3;

%% Initialization STEP 1
for i=1:a
    for j = 1:b
        for k = 1:c
            s = struct('W',randn(IP_N,1),'Theta',randn(3,1),'A',randn(3,3));
            WM3d{i,j,k} = s;
        end
    end
end
% Five parameters are required
% Sigma_0, Tau_sigma, Eta_0, Tau_eta, t
% Topological Neighbourhood
% T_j_I(x) = exp(-(S_j,I(x))^2/(2*Sigma(t)^2))
% where S_j,I(x) is distance between neuron i and j
% and Sigma(t) = Sigma_0*exp(-t/Tau_sigma)

% Weight Update Rule:
% Del_w_j_i = Eta(t)* T_j_I(x)*(x_i - w_j_i)
% Eta(t) = Eta_0*exp(-t/Tau_eta)
Sigma_0 = 12;
Tau_sigma = 500;
Eta_0 = 2;
Tau_eta = 500;
t = 1000;
no_of_iterations = t;
no_of_data = 1000;
% % MAIN LOOP STARTS
IP_matrix = []; % size = [IP_N, no_of_data]
for i=1:no_of_data
    IP_matrix = [IP_matrix,[200*randn(1,1)-100;300*randn(1,1)-150,200*randn(1,1)-100]]
end

for j0 = 1:no_of_iterations
    % TODO : update rule for Sigma_t
    Sigma_t = Sigma_0*exp(-no_of_iterations/Tau_sigma);
    
% TODO : update rule for Eta_t
    Eta_t = Eta_0*exp(-no_of_iterations/Tau_eta);

%% Sampling STEP 2
for i0=1:no_of_data
%% MATCHING STEP 3  finding the winning neuron
% We need to iterate through all the weight vectors corresponding to each
% neuron
Index_winning_neuron = [0,0,0];
Dummy_cost = Inf;
for i = 1:a
    for j = 1:b
        for k = 1:c
            temp_weight_vector = WM3d{i,j,k};
            tmp_similarity = norm(IP_matrix(:,i0)-temp_weight_vector);
            if(tmp_similarity < Dummy_cost)
                Dummy_cost = tmp_similarity;
                Index_winning_neuron = [i,j,k];
            end
        end
    end
end
% We need to calculate a radius with in which all neuron weight will be
% updated
% since T_j_I(x) is symmetric about I(x) the radius will depend upon the
% present Sigma(t) 
% We take the ball of size = sqrt(2)*Sigma(t)
% [p,q,r] = Index_winning_neuron;

r_ball = sqrt(2)*Sigma_t;
% TODO : update rule for Sigma_t
% TODO : update rule for Eta_t




% Hence initially Sigma(t) should be large max of {a,b,c}. After each loop
% through all the data it should decrease.
% hence the starting Sigma_0 = 12 (here)

% We need to find all the neighbourhood neuron along with the respective
% distance metric
% say the winning neuron has an index of (p,q,r) and the radius of the
% ball is r_ball. 

% Find list of all the index lying within a ball of radius r_ball and
% having center at (p,q,r)
%% WEIGHT UPDATING STEP 4
% Brute force search
for x = 1:a
    for y = 1:b
        for z = 1:c
            if(norm(Index_winning_neuron-[x,y,z]) <= r_ball)
                T_j_I = exp(-(norm(Index_winning_neuron-[x,y,z]))^2/(2*Sigma_t^2));
                tmp_weight_vector = WM3d{x,y,z};
                tmp_weight_vector = tmp_weight_vector + Eta_t* T_j_I*(IP_matrix(:,i0) - tmp_weight_vector);
            end
        end
    end
end
end
end
